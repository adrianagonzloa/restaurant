<?php
namespace Auth\Config;

// Create a new instance of our RouteCollection class.
$routes = \Config\Services::routes();

$routes->group('auth/user', ['namespace' => 'Modules\Auth\Controllers', 'filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->add('/', 'Users::index');
    $routes->get('edit/(:num)', 'Users::editView/$1');
    $routes->post('save/(:num)', 'Users::editAction/$1');
    $routes->get('delete/(:num)', 'Users::delete/$1');
    $routes->get('add/(:num)/(:alpha)', 'Users::addGroup/$1/$2');
    $routes->get('remove/(:num)/(:alpha)', 'Users::removeGroup/$1/$2');
});

<?php

namespace Modules\Auth\Controllers;

use App\Controllers\BaseController;

class Users extends BaseController{

    public function index(){
        $users = auth()->getProvider();
        $data['titulo'] = 'Listado de Usuarios';
        $data['users'] = $users->findAll();
        return view('Modules\Auth\Views\user\lista',$data);
    }
    
    public function editView($id){
        helper('form');
        $users = auth()->getProvider();
        $data['titulo'] = 'Editar Usuario';
        $data['user'] = $users->find($id);
        return view('Modules\Auth\Views\user\edita',$data);
    }
    
    public function editAction($id){
        $rules = $setting = setting('Validation.registration'); //tomo las reglas de validación del fichero app\Config\Validation.php
        //validamos antes de guardar - El password sólo se puede validar aquí.
        $datos = $this->request->getPost();
        $datos['id'] = $id;
        if (! $this->validateData($datos, $rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }
        
        // Save the user
        $allowedPostFields = array_keys($rules);
        $user              = new \CodeIgniter\Shield\Entities\User();
        $user->fill($this->request->getPost($allowedPostFields));
        $user->id=$id;
        
        echo '<pre>',$id,'<br>';
        print_r($user);
        $users = auth()->getProvider();
        try {
            $users->update($id, $user);
        } catch (ValidationException $e) {
            return redirect()->back()->withInput()->with('errors', $user->errors());
        }
        return redirect('auth/user');
    }
    
    public function delete($id){
        $users = auth()->getProvider();
        $users->delete($id, true);
        return redirect('auth/user');
    }
    
    public function addGroup($id, $grupo){
        $users = auth()->getProvider();
        $user = $users->find($id);
        $user->addGroup($grupo);
        return redirect('auth/user');
    }
    
    public function removeGroup($id, $grupo){
        $users = auth()->getProvider();
        $user = $users->find($id);
        $user->removeGroup($grupo);
        return redirect('auth/user');
    }
    
}

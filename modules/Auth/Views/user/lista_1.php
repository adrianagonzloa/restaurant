<!--</?= $this->extend('Modules\Auth\Views\templates\default') ?>-->

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('title')?>
    <?= $titulo ?>  <a href="<?=site_url('/')?>" title="Volver a Página Principal"><i class="bi bi-reply-all-fill"></i></a>
<?= $this->endSection() ?>

<?= $this->section('content')?>
    
    <table id="mytabla" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>E-mail</th>
                <th>Username</th>
                <th>Pertenece</th>
                <th>Grupos</th>
                <th>Acciones</td>
            </tr>
        </thead>
        <tbody>
        <?php $todos_grupos = array_keys(setting('AuthGroups.groups')); ?>    
        <?php foreach($users as $user): ?>
            <tr>
                <?php 
                       $pertenece = $user->getGroups();     
                       $no_pertenece = array_diff($todos_grupos, $pertenece); 
                    ?>
                <td><?= $user->lastname ?>, <?= $user->firstname ?></td>
                <td><?= $user->email ?> </td>
                <td><?= $user->username ?> </td>
                <td>
                     <?php foreach($pertenece as $grupo):?>
                    <a href="<?= site_url('auth/user/remove/'.$user->id.'/'.$grupo)?>">
                        <?= $grupo ?>
                    </a>    
                    <?php endforeach; ?>
                </td>
                <td>
                    <?php 
                       $todos_grupos = array_keys(setting('AuthGroups.groups'));
                       $pertenece = $user->getGroups();     
                       $no_pertenece = array_diff($todos_grupos, $pertenece); 
                    ?>
                    <?php foreach($no_pertenece as $grupo):?>
                    <a href="<?= site_url('auth/user/add/'.$user->id.'/'.$grupo)?>">
                        <?= $grupo ?>
                    </a>    
                    <?php endforeach; ?>
                </td>
                <td>
                    <a href="<?= site_url('auth/user/edit/'.$user->id)  ?>">
                        <span class="bi bi-pencil"></span>
                    </a>
                    <a href="<?= site_url('auth/user/delete/'.$user->id)  ?>" onclick="return confirm('Estás seguro de que quieres borrar el médico seleccionado')">
                        <span class="bi bi-trash text-danger"></span>
                    </a>
                </td>    
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    
    <?php // print_r(setting('AuthGroups.groups')); ?>
<?= $this->endSection()?>
        
<!DOCTYPE html>
<html>
    <head>
        <title>
            <?= $this->renderSection('title',true)?>
        </title>
        <link rel="icon" href="<?= base_url('assets/images/logo0.png') ?>">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap4.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
    </head>
    <body>
        <div class="container">
            <h2 class='text-danger'><?= $this->renderSection('title')?></h2>
            <?= $this->renderSection('content')?>
        </div>
        <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
        <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.js"></script>
        <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap4.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.js"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/scripts.js')?>"></script>    
    </body>
</html>


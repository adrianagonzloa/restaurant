<?php
/*******************************************************************************
 * $data = nombre (name) del elemento
 * $value =
 * $extra = [   'label' => '',
 *              'obligatorio' =>,
 *              'class' =>,
 *              'errors' =>.
 *          
 ******************************************************************************/


function my_form_input($name,$value,$extra){   
    $html =  form_label($extra['label'],$name);
    $options = ['class'=>$extra['class'], 'id'=>$name];
    if (isset($extra['errors']) AND isset($extra['errors'][$name])){
        $options['class'] .= ' error is-invalid';
        $options['aria-invalid']='true';
    }
    // Verifica un campo de lecturaq
    if (isset($extra['readonly']) && $extra['readonly'] === true) {
        $options['readonly'] = 'readonly';
    }
    $html .= form_input($name,set_value($name, $value),$options);
    if (isset($extra['errors']) AND isset($extra['errors'][$name])){
        $html .= form_label($extra['errors'][$name],$name.'-error',['class'=>'error is-invalid']);
    }
    return $html;
}

function my_form_select($name,$value,$extra){   
    $html =  form_label($extra['label'],$name);
    $options = ['class'=>$extra['class'], 'id'=>$name];
    if (isset($extra['errors']) AND isset($extra['errors'][$name])){
        $options['class'] .= ' error is-invalid';
        $options['aria-invalid']='true';
    }
    // Verificar un campo de selección
    if (isset($extra['options']) && is_array($extra['options'])) {
        // Generar solo el campo de selección si se proporcionan opciones
        $html .= form_dropdown($name, $extra['options'], $value, $options);
    } else {
        // Generar el campo de entrada por defecto si no se proporcionan opciones
        $html .= form_input($name, set_value($name, $value), $options);
    }
    
    if (isset($extra['errors']) AND isset($extra['errors'][$name])){
        $html .= form_label($extra['errors'][$name],$name.'-error',['class'=>'error is-invalid']);
    }
    return $html;
}

/*******************************************************************************
 * $data = nombre (name) del elemento
 * $value =
 * $extra = [   'label' => '',
 *              'obligatorio' =>,
 *              'class' =>,
 *              'errors' =>.
 *          
 ******************************************************************************/

function my_form_upload($name,$value,$extra){ 
    $html  = '<div class="form-group">';
    $html .=  form_label($extra['label'],$name,['class'=>'my-input-file']);
    $html .= '<div class="input-group">';
    $html .= '<div class="custom-file">';
    $options = ['class'=>$extra['class'], 'id'=>$name, 'lang'=> 'es', 'multiple' => 'multiple'];
    if (isset($extra['errors']) AND isset($extra['errors'][$name])){
        $options['class'] .= ' error is-invalid';
        $options['aria-invalid']='true';
    }
    
    $html .= form_upload($name.'[]',set_value($name, $value),$options);
    $html .= form_label('',$name,['class'=>'custom-file-label']);
    $html .= '</div>';
    $html .= '</div>';
    if (isset($extra['errors']) AND isset($extra['errors'][$name])){
        $html .= form_label($extra['errors'][$name],$name.'-error',['class'=>'error is-invalid']);
    }
    $html .= '</div>';
    return $html;  
}


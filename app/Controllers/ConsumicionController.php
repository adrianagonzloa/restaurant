<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\ConsumicionModel;
/**
 * Description of ConsumicionControler
 *
 * @author adriana
 */
class ConsumicionController extends BaseController{
// este controlador es para la tabla de consumicion
    public function index(){
	$consumiciones = new ConsumicionModel(); 
	$data['titulo'] = 'Lista de las Consumiciones'; //Esto se muestra en la vista
	$data['consumiciones'] = $consumiciones->findAll(); 
        return view('autorizados/consumicion/tabla',$data);
    }
    
    public function formconsumicion_editar($id){
        Helper('form');
        $data['titulo'] = 'Modificación de Consumicion';
        
        $consumicionModel = new ConsumicionModel(); //para acceder a la BD
        $data['consumicion'] = $consumicionModel->find($id);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/consumicion/formconsumicion_editar',$data);
        } else {
            $consumicion = $this->request->getPost();
            unset($consumicion['enviar']);
            $consumicion['id'] = $id;
            
            $consumicionModel = new ConsumicionModel(); //crear el objeto
            if ($consumicionModel->update($id,$consumicion)===false){
               //mostrar los errores
                $data['errors'] = $consumicionModel->errors();
                return view('autorizados/consumicion/formconsumicion_editar',$data); 
            } else {
                return redirect('consumiciones');
            }
        }
    }   

    
    public function borrarconsumicion($id){
        $consumicionModel = new ConsumicionModel(); //crear el objeto
        //borrar
        $consumicionModel
                ->where(['id'=>$id])
                ->delete();
        return redirect()->to('consumiciones');
    }
    
}

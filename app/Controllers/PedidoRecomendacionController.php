<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\PedidoRecomendacionModel;
/**
 * Description of PedidoRecomendacionControler
 *
 * @author adriana
 */
class PedidoRecomendacionController extends BaseController{
// este controlador es para la tabla de pedido_recomendacion
    public function index(){
	$pedido_recomendaciones = new PedidoRecomendacionModel();
	$data['titulo'] = 'Lista de las Recomendaciones de Pedidos';
	$data['pedido_recomendaciones'] = $pedido_recomendaciones->findAll(); 
        return view('autorizados/pedido_recomendacion/tabla',$data);
    }
    
    public function formpedrec_editar($cod_ped_rec){
        Helper('form');
        $data['titulo'] = 'Modificación de Pedido Recomendación';
        
        $pedidorecomendacionModel = new PedidoRecomendacionModel(); //para acceder a la BD
        $data['pedido_recomendacion'] = $pedidorecomendacionModel->find($cod_ped_rec);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/pedido_recomendacion/formpedrec_editar',$data);
        } else {
            $pedido_recomendacion = $this->request->getPost();
            unset($pedido_recomendacion['enviar']);
            $pedido_recomendacion['cod_ped_rec'] = $cod_ped_rec;
            
            $pedidorecomendacionModel = new PedidoRecomendacionModel(); //crear el objeto
            if ($pedidorecomendacionModel->update($cod_ped_rec,$pedido_recomendacion)===false){
               //quiero mostrar los errores
                $data['errors'] = $pedidorecomendacionModel->errors();
                return view('autorizados/pedido_recomendacion/formpedrec_editar',$data); 
            } else {
                return redirect('pedido_recomendaciones');
            }
        }
        
    }   

    
    public function borrarpedrec($cod_ped_rec){
        $pedidorecomendacionModel = new PedidoRecomendacionModel(); //crear el objeto
        //borrar
        $pedidorecomendacionModel
                ->where(['cod_ped_rec'=>$cod_ped_rec])
                ->delete();
        return redirect()->to('pedido_recomendaciones');

    }
    
    
}

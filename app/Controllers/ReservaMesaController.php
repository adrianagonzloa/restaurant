<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\ReservaMesaModel;
/**
 * Description of ReservaMesaControler
 *
 * @author adriana
 */
class ReservaMesaController extends BaseController{
// este controlador es para la tabla de reserva_mesa
    public function index(){
	$reserva_mesas = new ReservaMesaModel(); 
	$data['titulo'] = 'Lista de las Reservas de las mesas'; 
	$data['reserva_mesas'] = $reserva_mesas->findAll(); 
        return view('autorizados/reserva_mesa/tabla',$data);
    }
    
    public function formpreservamesa_editar($id){
        Helper('form');
        $data['titulo'] = 'Modificación de Reserva';
        
        $reservamesaModel = new ReservaMesaModel(); //para acceder a la BD
        $data['reserva'] = $reservamesaModel->find($id);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/reserva_mesa/formreservamesa_editar',$data);
        } else {
            $reserva_mesa = $this->request->getPost();
            unset($reserva_mesa['enviar']);
            $reserva_mesa['id'] = $id;
            
            $reservamesaModel = new ReservaMesaModel(); //crear el objeto
            if ($reservamesaModel->update($id,$reserva_mesa)===false){
               //mostrar los errores, que aun no preparado
                $data['errors'] = $reservamesaModel->errors();
                return view('autorizados/reserva_mesa/formreservamesa_editar',$data); 
            } else {
                return redirect('reserva_mesa');
            }
        }
        
    }   

    
    public function borrarreservamesa($id){
        $reservamesaModel = new ReservaMesaModel(); //crear el objeto
        //borrar
        $reservamesaModel
                ->where(['id'=>$id])
                ->delete();
        return redirect()->to('reserva_mesa');
    }
    
   public function form_fechahora(){
        helper('form');
        $data['title'] = 'Aquí puedes hacer tu reserva';
        return view('prueba/reservas', $data);
   }
   
   
   public function calendar(){
        $data['title'] = "Reserva de mesa";
        $data['fechas'] = ["2024-04-21","2024-04-26","2024-04-27"];
        return view('tempus/prueba',$data);
    }
   
}

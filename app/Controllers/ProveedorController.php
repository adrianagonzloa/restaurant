<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\ProveedorModel;
/**
 * Description of Proveedor
 *
 * @author adriana
 */
class ProveedorController extends BaseController{
// este controlador es para la tabla de proveedor
    public function index(){
	$proveedores = new ProveedorModel(); 
	$data['titulo'] = 'Lista de los Proveedores'; 
	$data['proveedores'] = $proveedores->findAll(); 
        return view('autorizados/proveedor/tabla',$data);
    }
    
    public function formproveedor_editar($cod_proveedor){
        Helper('form');
        $data['titulo'] = 'Modificación de Proveedor';
        
        $proveedorModel = new ProveedorModel(); 
        $data['proveedor'] = $proveedorModel->find($cod_proveedor);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/proveedor/formproveedor_editar',$data);
        } else {
            $proveedor = $this->request->getPost();
            unset($proveedor['enviar']);
            $proveedor['cod_proveedor'] = $cod_proveedor;
            
            $proveedorModel = new ProveedorModel(); //crear el objeto
            if ($proveedorModel->update($cod_proveedor,$proveedor)===false){
               //mostrar los errores
                $data['errors'] = $proveedorModel->errors();
                return view('autorizados/proveedor/formproveedor_editar',$data); 
            } else {
                return redirect('proveedores');
            }
        }
        
    }   

    
    public function borrarproveedor($cod_proveedor){
        $proveedorModel = new ProveedorModel(); //crear el objeto
        //borrar
        $proveedorModel
                ->where(['cod_proveedor'=>$cod_proveedor])
                ->delete();
        return redirect()->to('cod_proveedor');
    }
    
}

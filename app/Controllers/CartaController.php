<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\PlatoModel;
/**
 * Description of ClienteControler
 *
 * @author adriana
 */
class CartaController extends BaseController{
// este controlador sirve para el contenido de la carta
    public function postres(){
	$postres = new PlatoModel(); // use arriba hace que no se ponga aquí
	$data['postres'] = $postres->findAll(); 
        return view('publico/carta/postres', $data);
    } 
    // recoge del modelo la información de la tabla para mostrarla, con el anterior y los siguientes
    public function entrantes(){
        $entrantes = new PlatoModel(); 
	$data['entrantes'] = $entrantes->findAll(); 
        return view('publico/carta/entrantes', $data);
    }
    
    public function principal(){
        $principales = new PlatoModel(); // use arriba hace que no se ponga aquí
	$data['principales'] = $principales->findAll(); 
        return view('publico/carta/principal', $data);
    }     
    
    public function bebida(){
        $bebidas = new PlatoModel(); // use arriba hace que no se ponga aquí
	$data['bebidas'] = $bebidas->findAll(); 
        return view('publico/carta/bebida', $data);
    }  
    
}

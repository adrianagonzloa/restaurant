<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\ClienteModel;
/**
 * Description of ClienteControler
 *
 * @author adriana
 */
class ClienteController extends BaseController{
// este controlador la tabla de clientes
    public function index(){
	$clientes = new ClienteModel(); // use arriba hace que no se ponga aquí
	$data['titulo'] = 'Lista de los clientes'; //Esto se muestra en la vista
	$data['clientes'] = $clientes->findAll(); // Encuentra a todos los clientes
        return view('autorizados/z_bor/clientes/tabla',$data);//llama a la vista
    }
    
    
    public function formCliente_editar($cod_cliente){
        Helper('form');
        $data['titulo'] = 'Modificación Cliente';
        
        $clienteModel = new ClienteModel(); //para acceder a la base de datos
        $data['cliente'] = $clienteModel->find($cod_cliente);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/clientes/formcliente_editar',$data);
        } else {
            $cliente = $this->request->getPost();
            unset($cliente['enviar']);
            $cliente['cod_cliente'] = $cod_cliente;//Se pone el codigo, lo que se dice en el ClienteModel
            
            $clienteModel = new ClienteModel(); //crear el objeto
            if ($clienteModel->update($cod_cliente,$cliente)===false){
               //esto es para mostrar los errores pero todavia no esta
                $data['errors'] = $clienteModel->errors();
                return view('autorizados/clientes/formcliente_editar',$data); 
            } else {
                return redirect('clientes');
            }
        }
        
    }   

    
    public function borrarcliente($cod_cliente){
        $clienteModel = new ClienteModel(); //crear el objeto
        //y ahora se borra
        $clienteModel
                ->where(['cod_cliente'=>$cod_cliente])
                ->delete();
        return redirect()->to('clientes');

    }

    
    
}

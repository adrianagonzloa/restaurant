<?php

namespace App\Controllers;
use App\Models\ReservaMesaModel;
use App\Models\TurnoModel;

//use App\Models\AAReservaModel;
////use App\Models\AARecursoModel;
//use App\Models\AATurnoModel;
/**
 * Description of ReservaController
 *
 * @author adriana
 */
class ReservaController extends BaseController{
  
    //Con esto preguntamos la cantidad de personas que van a ser entre 1 y 6
    public function cantidadPersonas(){
        helper('form');
        $data['title'] = "Por favor indique la cantidad de personas";
        return view('reserva/formcantidadPersonas', $data);
    }
    
    //Lo que hace esto es recibir la cantidad de personas que serán, llama a la funcion de IdMesa
    //Sabremos el id y se lo pasamos a formReservar, que es el que mostrará la disponibilidad
    public function personasReserva(){
        $cantidadPersonas = $this->request->getPost('num_personas');
        
        // Saber el Id de la mesa según la cantidad de personas
        $idMesa = $this->IdMesa($cantidadPersonas);

        // Redirigir al otro controlador pasando el Id de la mesa
        return redirect()->to("reserva/form/$idMesa");
    }
    
    // Comprueba que mesa es dependiendo de la cantidad de personas
    public function IdMesa($cantidadPersonas){
        if ($cantidadPersonas <= 2) {
            return 1;
        } elseif ($cantidadPersonas <= 4) {
            return 2;
        } else {
            return 3;
        }
    }
    
    // Formulario que muestra dependiendo de la disponibilidad y permite reservar
    public function formReservar($mesa){
        
    	$reservamesaModel = new ReservaMesaModel();
    	$reservas = $reservamesaModel->getReservasMesa($mesa); //por defecto vemos las reservas para 2
    	$data['recurso'] = $mesa; 

        $data['fechas'] = array_column($reservas, 'fecha'); //le paso las fechas para que las deshabilite
    	//print_r($reservas);
    	helper('form');
    	$data['title'] = 'Reservas';
    	$data['hoy'] = date('Y-m-d');
    	$data['mesqueviene'] = date('Y-m-d',strtotime("+1 month", strtotime($data['hoy'])));
    	//echo '<pre>';
    	//print_r($data);
    	if (strtoupper($this->request->getMethod())==='GET'){
        	helper('form');
        	return view('reserva/formReserva',$data);
    	} else {
        	echo '<pre>';
        	print_r($this->request->getPost('tempus'));
    	}
   	 
    }
    
 
    protected $auth;

    public function __construct()
    {
        // Carga el servicio de autenticación
        $this->auth = service('authentication');
    }

        
    //Turnos libres encontrados
    public function turnosLibres(){ 
        // Verifica si el usuario está autenticado
        if (auth()->loggedIn()) {
            // Obtiene el usuario registrado
            $usuario = auth()->user()->id; // Suponiendo que el ID del usuario es "id"
        //echo print_r($usuario);
        }
    	helper('html');
    	$mesa = $this->request->getPost('recurso');
    	$fecha = $this->request->getPost('fecha'); //recogemos los dos parámetros como si viniera de un formulario
    	//le cambiamos el formato de dd/mm/yyyy a yyyy-mm-dd
    	$vector = explode('/', $fecha);
    	$fecha = $vector[2].'-'.$vector[1].'-'.$vector[0];
    	$turnoModel = new TurnoModel();
    	$reservamesaModel = new ReservaMesaModel();
    	$todos_turnos = $turnoModel->findAll();
        //echo '<pre>';
        //print_r($todos_turnos);
    	date_default_timezone_set("Europe/Madrid");
    	setlocale(LC_TIME, 'es_ES.UTF-8','esp');
    	$fecha_español = strtotime($fecha);
    	$html = "<div class='alert alert-secondary m-4 col-md-4'>".strftime('%A %e de %B de %Y',$fecha_español)."</div>";
    	foreach($todos_turnos as $turno){
        	if (!$reservamesaModel->estaTurnoCompleto($mesa, $fecha, $turno->cod_turno)){
           	 
            	$html.= anchor("reserva/$usuario/$mesa/$turno->cod_turno/$fecha",$turno->horario,['class'=>'btn btn-info m-2']);
        	}
    	}
    	return $html;
    }
    
    
    // Método para guardar la reserva en la base de datos
    public function guardarReserva($usuario,$mesa, $cod_turno, $fecha){
        // Guardar la reserva en la base de datos
        $reservaModel = new ReservaMesaModel();
        $reserva = [
            'cod_cliente' => $usuario,
            'cod_mesa' => $mesa,
            'fecha' => $fecha,
            'cod_turno' => $cod_turno
        ];

        if ($reservaModel->insert($reserva) === false) { //detecta si hay un error
                echo 'Algo ha ido masl, vuelva a intentarlo más tarde';    
        } else {
            // Si se inserta correctamente, redirigir a la página de entrada
            return redirect('reserva_mesa');
        }
    }
    
    public function ultima_reserva(){
        if (auth()->loggedIn()) {
            // Obtiene el usuario registrado
            $usuario = auth()->user()->id; // Suponiendo que el ID del usuario es "id"
        }else{
            return $this->response->setJSON(['success' => false, 'message' => 'Usuario no autenticado']);
        }
    	$hoy = date('Y-m-d');
        
        $reservaModel = new ReservaMesaModel();
        
        // Obtener la última reserva del usuario autenticado
        $ultimaReserva = $reservaModel
                ->select('reserva_mesa.id, reserva_mesa.fecha, turno.horario, turno.turno')
                ->join('turno', 'turno.cod_turno=reserva_mesa.cod_turno', 'LEFT')
                ->join('users', 'users.id=reserva_mesa.cod_cliente', 'LEFT')
                ->where('fecha >=', $hoy)
                ->where('cod_cliente', $usuario)
                ->where('estado', 'Confirmada')
                ->orderBy('fecha', 'desc')
                ->findAll();

        if ($ultimaReserva) {
            return $this->response->setJSON(['success' => true, 'reserva' => $ultimaReserva]);
        } else {
            return $this->response->setJSON(['success' => false, 'message' => 'No se encontró ninguna reserva']);
        }
    }
    
    
    public function cancelar_reserva(){
        $reservaId = $this->request->getPost('id');
        $reservaModel = new ReservaMesaModel();
        // Actualizar el estado de la reserva a 'Cancelada'
        $data = ['estado' => 'Cancelada'];
        $reservaModel->update($reservaId, $data);

        return $this->response->setJSON(['success' => true, 'message' => 'Reserva cancelada exitosamente']);
    }

}
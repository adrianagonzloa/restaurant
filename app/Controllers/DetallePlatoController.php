<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\DetallePlatoModel;
/**
 * Description of DetallePlatoControler
 *
 * @author adriana
 */
class DetallePlatoController extends BaseController{
// este controlador es para la tabla de detalle_plato
    public function index(){
	$detalle_platos = new DetallePlatoModel();
	$data['titulo'] = 'Lista de los detalles de los Platos'; 
	$data['detalle_platos'] = $detalle_platos->findAll(); 
        return view('autorizados/detalle_plato/tabla',$data);
    }
    
    public function formdetalleplato_editar($id){
        Helper('form');
        $data['titulo'] = 'Modificación de los detalles del plato';
        
        $detalleplatoModel = new DetallePlatoModel(); //para acceder a la BD
        $data['detalle_plato'] = $detalleplatoModel->find($id);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/detalle_plato/formdetalleplato_editar',$data);
        } else {
            $detalle_plato = $this->request->getPost();
            unset($detalle_plato['enviar']);
            $detalle_plato['id'] = $id;
            
            $detalleplatoModel = new DetallePlatoModel(); //crear el objeto
            if ($detalleplatoModel->update($id,$detalle_plato)===false){
               //mostrar los errores
                $data['errors'] = $detalleplatoModel->errors();
                return view('autorizados/detalle_plato/formdetalleplato_editar',$data); 
            } else {
                return redirect('detalle_plato');
            }
        }
    }   

    
    public function borrardetalleplato($id){
        $detalleplatoModel = new DetallePlatoModel(); //crear el objeto
        //borrar
        $detalleplatoModel
                ->where(['id'=>$id])
                ->delete();
        return redirect()->to('detalle_plato');
    }
}

<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\ProductoModel;
/**
 * Description of ProductoControler
 *
 * @author adriana
 */
class ProductoController extends BaseController{
// este controlador es para la tabla de producto
    public function index(){
	$productos = new ProductoModel(); 
	$data['titulo'] = 'Lista de los Productos'; 
	$data['productos'] = $productos->findAll();
        return view('autorizados/producto/tabla',$data);
    }
    
    public function formproducto_editar($cod_producto){
        Helper('form');
        $data['titulo'] = 'Modificación de Producto';
        
        $productoModel = new ProductoModel();
        $data['producto'] = $productoModel->find($cod_producto);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/producto/formproducto_editar',$data);
        } else {
            $producto = $this->request->getPost();
            unset($producto['enviar']);
            $producto['cod_producto'] = $cod_producto;
            
            $productoModel = new ProductoModel(); //crear el objeto
            if ($productoModel->update($cod_producto,$producto)===false){
               //quiero mostrar los errores
                $data['errors'] = $productoModel->errors();
                return view('autorizados/producto/formproducto_editar',$data); 
            } else {
                return redirect('productos');
            }
        }
        
    }   

    
    public function borrarproducto($cod_producto){
        $productoModel = new ProductoModel(); //crear el objeto
        //borrar
        $productoModel
                ->where(['cod_producto'=>$cod_producto])
                ->delete();
        return redirect()->to('cod_producto');
    }
    
    
}

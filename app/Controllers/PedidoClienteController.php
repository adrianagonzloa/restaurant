<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\PedidoClienteModel;
/**
 * Description of PedidoClienteControler
 *
 * @author adriana
 */
class PedidoClienteController extends BaseController{
// este controlador es para la tabla de pedido_cliente
    public function index(){
	$pedido_clientes = new PedidoClienteModel(); 
	$data['titulo'] = 'Lista de los pedidos de los clientes'; 
	$data['pedido_clientes'] = $pedido_clientes->findAll(); 
        return view('autorizados/pedido_cliente/tabla',$data);
    }
    
    public function formpedidocliente_editar($cod_pedido_cliente){
        Helper('form');
        $data['titulo'] = 'Modificación de Pedido Cliente';
        
        $pedidoclienteModel = new PedidoClienteModel(); 
        $data['pedido_cliente'] = $pedidoclienteModel->find($cod_pedido_cliente);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/pedido_cliente/formpedidocliente_editar',$data);
        } else {
            $pedido_cliente = $this->request->getPost();
            unset($pedido_cliente['enviar']);
            $pedido_cliente['cod_pedido_cliente'] = $cod_pedido_cliente;
            
            $pedidoclienteModel = new PedidoClienteModel(); //crear el objeto
            if ($pedidoclienteModel->update($cod_pedido_cliente,$pedido_cliente)===false){
               //quiero mostrar los errores
                $data['errors'] = $pedidoclienteModel->errors();
                return view('autorizados/pedido_cliente/formpedidocliente_editar',$data); 
            } else {
                return redirect('pedido_clientes');
            }
        }
        
    }   

    
    public function borrarpedidocliente($cod_pedido_clietne){
        $pedidoclienteModel = new PedidoClienteModel(); //crear el objeto
        //borrar
        $pedidoclienteModel
                ->where(['cod_pedido_cliente'=>$cod_pedido_clietne])
                ->delete();
        return redirect()->to('pedido_clientes');

    }
    
    
}

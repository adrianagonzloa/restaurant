<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\PedidoModel;
/**
 * Description of PedidoControler
 *
 * @author adriana
 */
class PedidoController extends BaseController{
// este controlador es para la tabla de pedido
    public function index(){
	$pedidos = new PedidoModel(); 
	$data['titulo'] = 'Lista de los Pedidos'; 
	$data['pedidos'] = $pedidos->findAll(); 
        return view('autorizados/pedido/tabla',$data);
    }
    
    
    public function formpedido_editar($cod_pedido){
        Helper('form');
        $data['titulo'] = 'Modificación de Pedido';
        
        $pedidoModel = new PedidoModel(); //para acceder a la BD
        $data['pedido'] = $pedidoModel->find($cod_pedido);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/pedido/formpedido_editar',$data);
        } else {
            $pedido = $this->request->getPost();
            unset($pedido['enviar']);
            $pedido['cod_pedido'] = $cod_pedido;
            
            $pedidoModel = new PedidoModel(); //crear el objeto
            if ($pedidoModel->update($cod_pedido,$pedido)===false){
               //quiero mostrar los errores
                $data['errors'] = $pedidoModel->errors();
                return view('autorizados/pedido/formmesa_editar',$data); 
            } else {
                return redirect('pedidos');
            }
        }
        
    }   

    
    public function borrarpedido($cod_pedido){
        $pedidoModel = new PedidoModel(); //crear el objeto
        //borrar
        $pedidoModel
                ->where(['cod_pedido'=>$cod_pedido])
                ->delete();
        return redirect()->to('pedidos');

    }
    
    
}

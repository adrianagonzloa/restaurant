<?php

namespace App\Controllers;

use App\Models\ReservaModel;
use App\Models\RecursoModel;

class MyHome extends BaseController
{
    public function index(): string
    {
        return view('welcome_message');
    }
    
    public function calendar(){
        $data['title'] = "Reserva de mesa";
        $data['fechas'] = ["2024-04-21","2024-04-26","2024-04-27"];
        return view('tempus/prueba',$data);
    }
    public function calendar6(){
        $data['title'] = "Reserva de mesa";
        $data['fechas'] = ["2024-04-21","2024-04-26","2024-04-27"];
        return view('tempus/prueba6',$data);
    }
    
    public function datatable(){
        $data['title'] = "Reserva de mesa";
        $data['fechas'] = ["2024-04-21","2024-04-26","2024-04-27"];
        return view('tempus/datatable',$data);
    }
    
    public function formTempus(){
        if (strtoupper($this->request->getMethod())==='GET'){
            helper('form');
            $data['title'] = "Seleccionar una fecha y hora"; 
            return view('tempus/formTempus',$data);
        } else {
            echo '<pre>';
            print_r($this->request->getPost('tempus'));
        }
    }
    
    public function getReservas(){
        $reservaModel = new ReservaModel();
        echo '<pre>';
        echo "<p>Reservas 1</p>";
        $reservas = $reservaModel->getReservas(1, '2024-05-07', '2024-05-22');
        print_r($reservas);
        echo "<p>Reservas 1</p>";
        $reservas = $reservaModel->getReservas(1, '2024-04-07', '2024-05-01');
        print_r($reservas);
        echo "<p>Reservas 1</p>";
        $reservas = $reservaModel->getReservas(1, '2024-05-17', '2024-06-22');
        print_r($reservas);
        echo "<p>Reservas 2</p>";
        $reservas = $reservaModel->getReservas(2, '2024-05-07', '2024-05-22');
        print_r($reservas);
        echo "<p>Reservas 2</p>";
        $reservas = $reservaModel->getReservas(2, '2024-05-07', '2024-05-20');
        print_r($reservas);
         echo "<p>Reservas 2</p>";
        $reservas = $reservaModel->getReservas(2, '2024-05-07', '2024-05-20');
        print_r($reservas);
    }
    
    public function getDisponibles(){
      
        $reservaModel = new ReservaModel();
        echo '<pre>';
        echo "<p>Disponible</p>";
        $reservas = $reservaModel->getDisponibles( '2024-05-09', '2024-05-20');
        print_r($reservas);
        
    }
    
    public function getLibres(){
        $reservaModel = new ReservaModel();
        $recursoModel = new RecursoModel();
        
        $disponibles=[];
        $recursos = $recursoModel->findAll();
        foreach ($recursos as $recurso){
            if (!$reservaModel->estaReservada($recurso->id, '2024-05-15', '2024-05-20')){
                $disponibles[] = $recurso;
            }
        }
        echo '<pre>';
        echo "<h2>Libres 2024-05-15 a 2024-05-20</h2>";
        print_r($disponibles);
    }
    
    public function buscar(){
    	helper('form');
    	$texto = $this->request->getPost('texto'); //parámetro de formulario
    	$recursoModel = new RecursoModel();
    	$registros = $recursoModel->like('descripcion',$texto,'both')->findAll();
    	$htmlSelect = form_dropdown('recurso',array_column($registros,'descripcion','id'),'',['size'=>4,'class'=>'form-control']);
    	return $htmlSelect;
    	//return json_encode($registros);
}
    public function form_ajax(){
    	helper('form');
    	$data['title'] = "Formulario prueba AJAX";
    	return view('ajax/form_busqueda',$data);
    }
}

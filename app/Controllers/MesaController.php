<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\MesaModel;
/**
 * Description of MesaControler
 *
 * @author adriana
 */
class MesaController extends BaseController{
// este controlador es para la tabla de mesa
    public function index(){
	$mesas = new MesaModel(); 
	$data['titulo'] = 'Lista de las Mesas'; 
	$data['mesas'] = $mesas->findAll(); 
        return view('autorizados/mesa/tabla',$data);
    }
    
    public function formmesa_editar($cod_mesa){
        Helper('form');
        $data['titulo'] = 'Modificación Mesa';
        
        $mesaModel = new MesaModel(); 
        $data['mesa'] = $mesaModel->find($cod_mesa);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/mesa/formmesa_editar',$data);
        } else {
            $mesa = $this->request->getPost();
            unset($mesa['enviar']);
            $mesa['cod_mesa'] = $cod_mesa;
            
            $mesaModel = new MesaModel(); //crear el objeto
            if ($mesaModel->update($cod_mesa,$mesa)===false){
               //quiero mostrar los errores
                $data['errors'] = $mesaModel->errors();
                return view('autorizados/mesa/formmesa_editar',$data); 
            } else {
                return redirect('mesas');
            }
        }
        
    }   

    
    public function borrarmesa($cod_mesa){
        $mesaModel = new MesaModel(); //crear el objeto
        //borrar
        $mesaModel
                ->where(['cod_mesa'=>$cod_mesa])
                ->delete();
        return redirect()->to('mesas');

    }
    
}

<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\PlatoModel;
/**
 * Description of PlatoControler
 *
 * @author adriana
 */
class PlatoController extends BaseController{
// este controlador es para la tabla de plato
    public function index(){
	$platos = new PlatoModel(); 
	$data['titulo'] = 'Lista de los Platos';
	$data['platos'] = $platos->findAll();
        return view('autorizados/plato/tabla',$data);
    }
    
    
    public function formplato_editar($cod_plato){
        Helper('form');
        $data['titulo'] = 'Modificación de Plato';
        
        $platoModel = new PlatoModel(); 
        $data['plato'] = $platoModel->find($cod_plato);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/plato/formplato_editar',$data);
        } else {
            $plato = $this->request->getPost();
            unset($plato['enviar']);
            $plato['cod_pedido_cliente'] = $cod_plato;
            
            $platoModel = new PlatoModel(); 
            if ($platoModel->update($cod_plato,$plato)===false){
               //quiero mostrar los errores
                $data['errors'] = $platoModel->errors();
                return view('autorizados/plato/formplato_editar',$data); 
            } else {
                return redirect('platos');
            }
        }
        
    }   

    
    public function borrarplato($cod_plato){
        $platoModel = new PlatoModel(); //crear el objeto
        //borrar
        $platoModel
                ->where(['cod_plato'=>$cod_plato])
                ->delete();
        return redirect()->to('cod_plato');

    }
    
}

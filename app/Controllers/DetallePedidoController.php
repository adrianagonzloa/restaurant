<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\DetallePedidoModel;
/**
 * Description of DetallePedidoControler
 *
 * @author adriana
 */
class DetallePedidoController extends BaseController{
// este controlador es para la tabla de detalle_pedido
    public function index(){
	$detalle_pedidos = new DetallePedidoModel(); 
	$data['titulo'] = 'Lista de los detalles de los Pedidos'; 
	$data['detalle_pedidos'] = $detalle_pedidos->findAll(); 
        return view('autorizados/detalle_pedido/tabla',$data);
    }
    
    public function formdetallepedido_editar($id){
        Helper('form');
        $data['titulo'] = 'Modificación de los detalles del pedido';
        
        $detallepedidoModel = new DetallePedidoModel(); 
        $data['detalle_pedido'] = $detallepedidoModel->find($id);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/detalle_pedido/formdetallepedido_editar',$data);
        } else {
            $detalle_pedido = $this->request->getPost();
            unset($detalle_pedido['enviar']);
            $detalle_pedido['id'] = $id;
            
            $detallepedidoModel = new DetallePedidoModel(); //crear el objeto
            if ($detallepedidoModel->update($id,$detalle_pedido)===false){
               //mostrar los errores, pero aun no esta especificado en el modelo
                $data['errors'] = $detallepedidoModel->errors();
                return view('autorizados/detalle_pedido/formdetallepedido_editar',$data); 
            } else {
                return redirect('detalle_pedido');
            }
        }
    }   

    
    public function borrardetallepedido($id){
        $detallepedidoModel = new DetallePedidoModel(); //crear el objeto
        //borrar
        $detallepedidoModel
                ->where(['id'=>$id])
                ->delete();
        return redirect()->to('detalle_pedido');
    }
}

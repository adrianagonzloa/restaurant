<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index(): string
    {
        return view('welcome_message');
    }

/************************************************
**************Páginas sin restricción************
*************************************************/
    public function paginainicial(){//El home que es la pagina principal
            return view('publico/home');
    }
    
    public function contacto(){//Una página con algunos detalles del contacto
            return view('publico/contacto');
    }
    
    public function carta(){//La carta completa sin detalles
        //$data['title'] = 'Disfruta de nuestra deliciosa carta';
            return view('publico/carta/carta'); //, $data);
    }
       
    
/******************************************************************************
* Ejemplo de formulario utilizando datetime picker
*****************************************************************************/
    public function form_fechahora(){//es la reserva
        helper('form');
        $data['title'] = 'Aquí puedes hacer tu reserva';
        return view('publico/reservas', $data);
    }  
    

/******************************************************************************
* Dirige a la pagina "empleados", solo los que están en cierto grupo lo veran
*****************************************************************************/
    public function autorizados(){
        $data['title'] = 'Solo personas autorizadas tienen acceso';
            return view('autorizados/empleados', $data);
    }
    
/******************************************************************************
* Dirige a la pagina "empleados", solo los que están en cierto grupo lo veran
*****************************************************************************/
    public function solicitud(){
        $data['title'] = 'Solo personas autorizadas tienen acceso';
            return view('common/solicitud', $data);
    }
    public function erimagen(){
	$data['titulo'] = 'Entidad-Relación'; //Esto se muestra en la vista
        return view('er/imagen',$data);//llama a la vista
    }
    
    
    
}

<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\TipoMesaModel;
/**
 * Description of TipoMesaControler
 *
 * @author adriana
 */
class TipoMesaController extends BaseController{
// este controlador es para la tabla de tipo_mesa
    public function index(){
	$tipo_mesas = new TipoMesaModel(); 
	$data['titulo'] = 'Lista de los Tipos de Mesas'; 
	$data['tipo_mesas'] = $tipo_mesas->findAll(); 
        return view('autorizados/tipo_mesa/tabla',$data);
    }
    
    public function formtipomesa_editar($capacidad){
        Helper('form');
        $data['titulo'] = 'Modificación de Tipo Mesa';
        
        $tipomesaModel = new TipoMesaModel(); //para acceder a la BD
        $data['tipo_mesa'] = $tipomesaModel->find($capacidad);
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('autorizados/tipo_mesa/formtipomesa_editar',$data);
        } else {
            $tipo_mesa = $this->request->getPost();
            unset($tipo_mesa['enviar']);
            $tipo_mesa['capacidad'] = $capacidad;
            
            $tipomesaModel = new TipoMesaModel(); //crear el objeto
            if ($tipomesaModel->update($capacidad,$tipo_mesa)===false){
               //mostrar los errores
                $data['errors'] = $tipomesaModel->errors();
                return view('autorizados/tipo_mesa/formtipomesa_editar',$data); 
            } else {
                return redirect('tipo_mesa');
            }
        }
        
    }    

    
    public function borrartipomesa($capacidad){
        $tipomesaModel = new TipoMesaModel(); //crear el objeto
        //borrar
        $tipomesaModel
                ->where(['capacidad'=>$capacidad])
                ->delete();
        return redirect()->to('capacidad');
    }
    
}

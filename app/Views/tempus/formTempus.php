<?php
/*******************************************************************************
 * Ejemplo para ver como funciona Tempus-Dominus
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css')?>
	<?= $this->include('tempus/tempus_css') ?>
<?= $this->endSection()?>

<?= $this->section('js')?>
	<?= $this->include('tempus/tempus_js') ?>
<?= $this->endSection()?>

<?= $this->section('page_title') ?>
	<?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div class="container">
        <?= form_open('/tempus/form') ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                            <input type="text" name="tempus" class="form-control datetimepicker-input" data-target="#datetimepicker1"/>
                            <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= form_submit('Enviar','enviar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>
    </div>
<?= $this->endSection() ?>
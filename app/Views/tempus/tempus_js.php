<!-- Moment.js -->
<script src="<?=base_url('assets/js/moment/moment.js')?>" ></script>

<!-- Para trabajar con locale -->
<script src="<?=base_url('assets/js/moment/moment-with-locales.js')?>"></script>
<!-- El locale es del español -->
<script src="<?=base_url('assets/js/moment/locale/es.js')?>"></script>

<!-- Tempus-Dominus v5.39 -->
<script src="<?=base_url('assets/js/tempus-dominus/tempusdominus-bootstrap-4.js')?>" ></script>

<script type="text/javascript">
    $(function () {
    	$('#datetimepicker1').datetimepicker({
        	locale: 'es',  //español
        	daysOfWeekDisabled: [1], //lunes descanso
                disabledDates: [
                    	"05/08/2024", //formato americano mes/dia/año
                    	"05/15/2024",
                	],
                icons: {
                	time: "fa fa-clock",
                	date: "fa fa-calendar-days text-primary",
                	up: "fa-solid fa-caret-up text-primary",
                	down: "fa-solid fa-caret-down text-primary"
            	},
                maxDate: "2024-05-25",
        	minDate: "2024-05-04",  
                
    	});
    });
 </script>
<?php 
/*******************************************************************************
 * Ejemplo de prueba de tempus dominous
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css')?>
    <?php echo $this->include('tempus/prueba_css') ?>
<?= $this->endSection()?>

<?= $this->section('js')?>
    <?= $this->include('tempus/prueba_js') ?>
<?= $this->endSection()?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div style="overflow:hidden;background-color: antiquewhite;">
        <div class="form-group">
            <div class="row">
                <div class="col-md-8">
                    <div id="datetimepicker1">

                    </div>
                </div>
            </div>
        </div>

        <!-- Aquí muestra la fecha seleccionada -->
        <div id="fecha_seleccionada"></div>
    </div>
    
<?= $this->endSection() ?>

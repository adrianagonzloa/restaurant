<script src="<?=base_url('assets/js/moment/moment.js')?>" ></script>

<script src="<?=base_url('assets/js/moment/moment-with-locales.min.js')?>"></script>

<script src="<?=base_url('assets/js/moment/locale/es.js')?>"></script>

<script src="<?=base_url('assets/js/tempus-dominus/tempusdominus-bootstrap-4.js')?>" ></script>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            locale: 'es',  //español
            format: 'L', //sólo días LT días y horas
            daysOfWeekDisabled: [1], //lunes descanso
            disabledDates: [
                <?php foreach ($fechas as $fecha):?>
                    "<?=$fecha?>", 
                <?php endforeach; ?>
            ],
            inline: true,
            sideBySide: true,
            maxDate: "2024-05-21",
            minDate: "2024-04-19",
        });
        
        $('td:not(.disabled)').click(function(e) {
                e.preventDefault(); // Previene el envío del formulario por defecto

                var selectedDate = $(this).data('day');

                // Muestra en texto la fecha enviada
                $('#fecha_seleccionada').text('Fecha seleccionada: ' + selectedDate);

                return false; // Evita que el formulario se envíe
            });
    });
 </script>
        

<!-- Moment.js -->
<script src="<?=base_url('assets/js/moment/moment.js')?>" ></script>

<!-- Para trabajar con locale -->
<script src="<?=base_url('assets/js/moment/moment-with-locales.js')?>"></script>
<!-- El locale es del español -->
<script src="<?=base_url('assets/js/moment/locale/es.js')?>"></script>

<!-- Tempus-Dominus v5.39 -->
<script src="<?=base_url('assets/js/tempus-dominus/tempusdominus-bootstrap-4.js')?>" ></script>

<script type="text/javascript">
	$(function () {
            $('#datetimepicker1').datetimepicker({
                    locale: 'es',  //español
                    daysOfWeekDisabled: [1], //lunes descanso
                    disabledDates: [
                    <?php foreach ($fechas as $fecha):?>
                            "<?=$fecha?>",
                    <?php endforeach; ?>
                    ],
                    icons: {
                            time: "fa fa-clock",
                            date: "fa fa-calendar-days text-primary",
                            up: "fa-solid fa-caret-up text-primary",
                            down: "fa-solid fa-caret-down text-primary"
                    },
                    minDate: "<?=$hoy?>",
                    maxDate: "<?=$mesqueviene?>",
                    format: 'L',
                    inline: true,
                    sideBySide: true,
            });
        
            $('td:not(.disabled)').click(function(e) {
            	e.preventDefault(); // Previene el envío del formulario por defecto

            	var selectedDate = $(this).data('day');
            	$.ajax({
       		 url: "<?= site_url('reservas/turnoslibres')?>",
       		 method:  'POST',
       		 dataType: 'html',
       		 data:{"recurso": <?= $recurso ?>, "fecha": selectedDate},
       		 headers: {'X-Requested-With': 'XMLHttpRequest'},
       		 success: function(data) {
           		 // Update the content on success
           		 $("#fecha_seleccionada").html(data);
           		 },
       		 error: function(error) {
       		 // Handle errors
           		 console.error('Error:', error);
           		 }
   		 });

            	return false; // Evita que el formulario se envíe
    	});
            
            $('form').submit(function(e) {
                e.preventDefault(); // Previene el envío del formulario por defecto

                var selectedDate = $('#datetimepicker1').datetimepicker('date');

                // Muestra en texto la fecha enviada
                $('#fecha_seleccionada').text('Fecha seleccionada: ' + selectedDate.format('YYYY-MM-DD HH:mm:ss'));

                return false; // Evita que el formulario se envíe
            });
        });
        
 </script>
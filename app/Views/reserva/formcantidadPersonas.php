<!-- Formulario de reserva, hay que escoger la cantidad de personas -->
<!-- Botón de reservas próximas -->
<?= $this->extend('plantillas/plantilla_public') ?>

<?= $this->section('js') ?>
    <?= $this->include('reserva/mireserva_js') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div class="text-center text-black" id="home">
        <div class="mb-5">
            <h1 style="color: #009c8c; font-size: 50px"><strong> Bienvenido a la zona de reservas </strong></h1>
            <p style="font-size: 20px; color: black;"> ¿Cuántas personas vais a ser? </p>
        </div>
    </div>  
    <div class="container text-center">
        <?= form_open('/reserva/elegir_mesa') ?>
            <label for="num_personas">Número de personas:</label>
            <select name="num_personas" id="num_personas" class="btn btn-light">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
            </select>

            <?= form_submit('Enviar', 'enviar', ['class' => 'btn btn-primary']) ?>
        <?= form_close() ?>
    </div>

<!-- Botón para ver las próximas reservas qu ehaya realizado el cliente -->
    <div class="text-center text-black mt-5 pt-5" id="home">
        <div class="mb-5">
            <h1 style="color: #009c8c; font-size: 45px"><strong> Tus reservas </strong></h1>
            <p style="font-size: 20px; color: black;"> ¿Quieres saber cuál a sido tu última reserva?</p>
        </div>
    </div>
    <div class="text-center text-black">
        <!-- Botón para obtener la última reserva -->
        <button id="mireserva" class="btn btn-secondary">Ver última reserva</button>
        <div id="ultima_reserva" style="margin-top: 20px;"></div>
    </div>
<?= $this->endSection() ?>

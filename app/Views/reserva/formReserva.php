<!-- Formulario de reserva, aparece un calendario en el que puedes escoger un día -->
<?= $this->extend('plantillas/plantilla_public') ?>

<?= $this->section('css')?>
	<?= $this->include('reserva/formReserva_css') ?>
<?= $this->endSection()?>

<?= $this->section('js')?>
	<?= $this->include('reserva/formReserva_js') ?>
<?= $this->endSection()?>

<?= $this->section('page_title') ?>
	<?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div class="text-center text-black mb-5" id="home">
        <div>
            <h1 style="color: #009c8c; font-size: 50px"><strong> Seleccione la fecha </strong></h1>
            <p style="font-size: 20px;"> Seleccione la fecha y turnos deseados </p>
        </div>
    </div>  
    <div class="container">
        <?= form_open('/reserva/form') ?>
        <div class="row">
            <div style="overflow:hidden;">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md">
                            <div id="datetimepicker1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?= form_close() ?>
        <!-- Aquí muestra la fecha seleccionada -->
        <div id="fecha_seleccionada"></div>
    </div>
<?= $this->endSection() ?>

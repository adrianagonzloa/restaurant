<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css')?>
	<?php echo $this->include('tempus/tempus_css') ?>
<?= $this->endSection()?>

<?= $this->section('js')?>
	<?= $this->include('reserva/formReserva1_js') ?>
<?= $this->endSection()?>

<?= $this->section('page_title') ?>
	<?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
    <?= form_open('/reservas/form') ?>
    <div class="row">
        <div style="overflow:hidden;background-color: antiquewhite;">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-8">
                        <div id="datetimepicker1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <?= form_submit('Enviar', 'enviar', ['class' => 'btn btn-primary']) ?>
    <?= form_close() ?>
    <!-- Aquí muestra la fecha seleccionada -->
    <div id="fecha_seleccionada" class="bg-light"></div>
</div>

<?= $this->endSection() ?>

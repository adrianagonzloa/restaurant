<?php 
/*******************************************************************************
 * Tabla por cliente 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla_public') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
<div class="table-responsive m-5 p-5 bg-light rounded-4">
    <table id="myTable" class="table table-striped table-bordered bg-white"> 
        <thead>
            <tr>
                <th>Código</th>
                <th>Fecha</th>
                <th>Horario</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($clientes as $cliente): ?> 
            	<tr>
                    <td><?= $cliente-> id ?></td>
                    <td><?= $cliente-> fecha ?></td>
                    <td><?= $cliente->horario ?></td>
                    
            	</tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->endSection() ?>

<script src="<?= base_url('assets/js/moment.min.js') ?>"></script>
<script src="<?= base_url('assets/js/moment-with-locales.min.js') ?>"></script>
<script src="<?= base_url('assets/js/tempusdominus-bootstrap-4.min.js') ?>"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript">
    function reserva_cancelar(reserva_id) {
        $.ajax({
            url: '/reserva/cancelar_reserva',
            method: 'POST',
            data: { id: reserva_id },
            success: function(response) {
                if(response.success) {
                    $('#ultima_reserva').html(`<p>La reserva ha sido cancelada exitosamente.</p>`);
                } else {
                    $('#ultima_reserva').html(`<p>Error al cancelar la reserva.</p>`);
                }
            },
            error: function() {
                $('#ultima_reserva').html(`<p>Error al procesar la solicitud de cancelación.</p>`);
            }
        });
    }

    $(document).ready(function(){
        $('#mireserva').click(function(){
            $.ajax({
                url: '/reserva/ultima_reserva',
                method: 'POST',
                data: {},
                success: function(response){
                    if(response.success){
                        $('#ultima_reserva').empty();
                        response.reserva.forEach(reserva => {
                            $('#ultima_reserva').append(`
                                <div class="mb-3" id="reserva_${reserva.id}">
                                    <p>
                                        <strong>Reserva:</strong> 
                                        Fecha: ${reserva.fecha}, 
                                        Turno: ${reserva.turno}, 
                                        Horario del turno: ${reserva.horario}
                                        <button class="btn bi bi-x-square-fill text-danger" onclick="reserva_cancelar(${reserva.id})" data-toggle="tooltip" title="Cancelar reserva"></button>
                                    </p>
                                </div>
                            `);
                        });
                    } else {
                    if (response.message === 'Usuario no autenticado') {
                        $('#ultima_reserva').html(`
                            <p>Por favor, inicie sesión para ver su última reserva.</p>
                            <div class="mb-4">    
                                <a href="<?= base_url('login')?>">
                                    <button class="btn text-white" type="button" style="background-color: #009c8c;"> Inicia sesión </button>
                                </a>
                            </div>
                        `);
                    } else {
                        $('#ultima_reserva').html(`<p>No se encontró ninguna reserva.</p>`);
                    }
                    }
                },
                error: function(){
                    $('#ultima_reserva').html(`<p>Error al obtener la información de la reserva.</p>`);
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function(){
       $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<?php
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla y AJAX
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js')?>
	<?= $this->include('ajax/form_busqueda_js') ?>
<?= $this->endSection()?>

<?= $this->section('page_title') ?>
	<?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container">
	<?= form_open('') ?>
	Nombre: <input name="texto" type="text" onkeyup="buscaSugerencia(this.value)">

	<!-- en span id="textoBuscar" si pintan las sugerencias -->
	<p>Sugerencia: <span id="textoBuscar"></span></p>
    
	<?= form_submit('Enviar','enviar',['class'=>'btn btn-primary']) ?>
	<?= form_close() ?>
</div>
<?= $this->endSection() ?>
<script type="text/javascript">  
    function buscaSugerencia(texto) {
        	$.ajax({
            	url: "<?= site_url('ajax/buscar')?>",
            	method:  'POST',
            	dataType: 'html',
            	data:{"texto": texto},
            	headers: {'X-Requested-With': 'XMLHttpRequest'},
            	success: function(data) {
                	// Update the content on success
                	$("#textoBuscar").html(data);
                	},
            	error: function(error) {
            	// Handle errors
                	console.error('Error:', error);
                	}
        	});
    	}
</script>
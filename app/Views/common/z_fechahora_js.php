<script src="<?= base_url('assets/js/moment.min.js') ?>"></script>
<script src="<?= base_url('assets/js/moment-with-locales.min.js') ?>"></script>
<script src="<?= base_url('assets/js/tempusdominus-bootstrap-4.min.js') ?>"></script>
<script type="text/javascript">
        $(function () {
            $('#datetimepicker13').datetimepicker({
                inline: true,
                sideBySide: true
            });

            // Recoge la fecha seleccionada al pulssar en "Enviar"
            $('form').submit(function(e) {
                e.preventDefault(); // Previene el envío del formulario por defecto

                var selectedDate = $('#datetimepicker13').datetimepicker('date');

                // Muestra en texto la fecha enviada
                $('#fecha_seleccionada').text('Fecha seleccionada: ' + selectedDate.format('YYYY-MM-DD HH:mm:ss'));

                return false; // Evita que el formulario se envíe
            });
            
            $('td:not(.disabled)').click(function(e) {
                e.preventDefault(); // Previene el envío del formulario por defecto

                var selectedDate = $(this).data('day');

                // Muestra en texto la fecha enviada
                $('#fecha_seleccionada').text('Fecha seleccionada: ' + selectedDate);

                return false; // Evita que el formulario se envíe
            });
        });
</script>
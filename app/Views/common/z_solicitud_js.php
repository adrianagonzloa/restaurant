<script src="<?= base_url('assets/js/jquery.validate.min.js') ?>"></script>
<script src="<?= base_url('assets/js/additional-methods.min.js') ?>"></script>

<script src="<?= base_url('assets/js/localization/messages_es.js') ?>"></script>
<script>
    $.validator.addMethod('customphone', function (value, element) {
            return this.optional(element) || /^\d{9}$/.test(value);
    }, "Por favor entre un número de teléfono válido 9 dígitos");
    $.validator.addMethod("exactlength", function(value, element, param) {
        return this.optional(element) || value.length == param;
    }, $.validator.format("Por favor entre un número de teléfono válido {0} dígitos"));
    $(document).ready(function () {
        
          // Initialize form validation on the registration form.
          // It has the name attribute "registration"
        $("#solicitud").validate({
            errorClass: "error is-invalid",
            validClass: "valid is-valid",

          // Specify validation rules
            rules: {
              // The key name on the left side is the name attribute
              // of an input field. Validation rules are defined
              // on the right side
                dni: "required",
                nombre: {
                    required: true,
                    minlength: 3
                },    
                apellido1: "required",
                telefono: {
                    required: true,
                    digits: true,
                    exactlength: 9
                    //customphone: true //nombre del elemento y del método creado
                },
                email: {
                     required: true,
                        // Specify that email should be validated
                        // by the built-in "email" rule
                     email: true
                },
                email_confirm: {
                     required: true,
                     email: true,
                     equalTo: "#email"
                },
                password: {
                     required: true,
                     minlength: 5
                },
                'anexoIV[]': {
                    required: true,
                    extension: "pdf"
                },
                'copia_dni[]': {
                    required:true,
                    extension: "pdf"
                },
                declaracion_responsable: {
                    required:true,
                    extension: "pdf"
                },
                justificante: {
                    required:true,
                    extension: "pdf"
                },
                exenciones: {
                    extension: "pdf"
                }
            },
            
            errorPlacement: function(error, element) {
                if (element.is("input[type='file']")){
                    error.appendTo(element.parent("div").parent("div").parent("div"));
                } else {
                    error.insertAfter(element);
                }
            },

            
// Make sure the form is submitted to the destination defined
// in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
       });
    });
</script>

<!-- Sidebar -->

<a href="<?= base_url('/')?>" class="brand-link">
    <img src="<?= base_url('assets/images/logo0.png')?>" alt="Logo Armony Restaurante" class="brand-image img-circle elevation-3" style="opacity: .8;">
    <span class="brand-text font-weight-light">Armony Restaurante</span>
</a>

<div class="sidebar">

    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="<?= base_url('assets/images/logo-chef2.png')?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <?php if (auth()->loggedIn()): ?>
                <a href="logout" class="d-block"><?=auth()->user()->username ?></a>
            <?php else: ?>
                <a href="login" class="d-block">Autenticar</a>
            <?php endif ?>
            <!--<a href="#" class="d-block">Adriana Gonzalez Loaiza</a>
        -->
        </div>
    </div>

    <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
        </div>
        <div class="sidebar-search-results">
            <div class="list-group">
                <a href="#" class="list-group-item">
                    <div class="search-title">
                        <strong class="text-light"></strong>
                        N
                        <strong class="text-light"></strong>
                        o
                        <strong class="text-light"></strong>
                        <strong class="text-light"></strong>
                        e
                        <strong class="text-light"></strong>
                        l
                        <strong class="text-light"></strong>
                        e
                        <strong class="text-light"></strong>
                        m
                        <strong class="text-light"></strong>
                        e
                        <strong class="text-light"></strong>
                        n
                        <strong class="text-light"></strong>
                        t
                        <strong class="text-light"></strong>
                        <strong class="text-light"></strong>
                        f
                        <strong class="text-light"></strong>
                        o
                        <strong class="text-light"></strong>
                        u
                        <strong class="text-light"></strong>
                        n
                        <strong class="text-light"></strong>
                        d
                        <strong class="text-light"></strong>
                        !
                        <strong class="text-light"></strong>
                    </div>
                    <div class="search-path">    
                    </div>
                </a>  
            </div>
        </div>
        
    </div>

    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

<!--            <li class="nav-item">
                <a href="</?= base_url('reserva')?>" class="nav-link">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Reservas
                         <span class="right badge badge-danger">New</span> 
                    </p>
                </a>
            </li>-->
<!--            <li class="nav-item">
                <a href="</?= base_url('reserva/elegir_personas')?>" class="nav-link">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Reserva
                    </p>
                </a>
            </li>-->
<!--            <li class="nav-item">
                <a href="</?= base_url('carta')?>" class="nav-link">
                    <i class="nav-icon fas fa-utensils"></i>
                    <p>
                        Carta
                    </p>
                </a>
            </li>-->
            <?php if (auth()->loggedIn() AND auth()->user()->inGroup('superadmin')): ?>
            <li class="nav-header">Acceso Restringido</li>
            <li class="nav-item">
                <a href="<?= base_url('empleados')?>" class="nav-link">
                    <i class="nav-icon fas fa-table"></i>
                    <p>
                        Tablas
                        <!-- <span class="right badge badge-warning">New</span> -->
                    </p>
                </a>
            </li>
            <?php endif ?>
            <li class="nav-item">
                <a href=<?= base_url("erimagen")?> class="nav-link">
                    <i class="nav-icon fas fa-exchange"></i>
                    <p>
                        ER
                    </p>
                </a>
            </li>
        </ul>
        
    </nav>

</div>




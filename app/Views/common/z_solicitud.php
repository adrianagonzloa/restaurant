<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('templates/page') ?>

<?= $this->section('js')?>
    <?= $this->include('common/files_js') ?>
    <?php echo $this->include('pruebasacceso/solicitud_js') ?>
<?= $this->endSection()?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

<!-- Mostrar los errores si los hubiera -->
<?php if (isset($errors)) : ?>

<?php else: ?>
   <?php $errors=[] ?>
<?php endif ?>

<?= form_open_multipart('pruebaacceso/'.$tipo, ['id'=>'solicitud']) ?>
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Datos Personales</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class= "col-md-3 col-12">
                    <?= my_form_input('dni','',['label' => 'DNI/NIE: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
                </div>
                <div class="col-3 col-md-3 col-12">
                    <?= my_form_input('nombre','',['label' => 'Nombre: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
                </div>
                <div class="col-md-3 col-12">
                   <?= my_form_input('apellido1','',['label' => '1º Apellido: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
                </div>
                <div class="col-md-3 col-12">
                    <?= my_form_input('apellido2','',['label' => '2º Apellido:' ,'class'=>'form-control', 'errors'=>$errors])?>
                </div>

                <div class=" col-md-2 col-12">
                    <?= my_form_input('telefono','',['label' => 'Teléfono: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
                </div>
                <div class=" col-md-<?= $tipo=='S' ? '4' : '5'?> col-12">
                    <?= my_form_input('email','',['label' => 'Email: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
                </div>
                <div class="col-md-<?= $tipo=='S' ? '4' : '5'?> col-12">
                    <?= my_form_input('email_confirm','',['label' => 'Repite Email: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
                </div>
                <?php if($tipo=='S'):?>
                <div class=" col-md-2 col-12">
                    <label for="modalidad" class="label-form">Opción: <code>*</code></label>
                    <?= form_dropdown('modalidad',
                            ['A'=>'Humanidades y Ciencias Sociales', 'B' => 'Tecnología', 'C' => "Ciencias"],
                            set_value('modalidad','C'),['id'=>'modalidad', 'class'=>'form-control']) ?>
                </div>
                <?php endif ?>
                <div class="col-12">
                    <code class="small"> * obligatorio</code>
                </div>
            </div>
        </div>

    </div>
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Adjuntar Anexos y Documentación</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <?= my_form_upload('anexoIV','',['label' => 'Anexo IV: Solicitud de la PAC de Grado Superior <code>*</code><div class="small">Solicitud firmada y escaneada. Archivo en formato pdf.</div>' ,'class'=>'', 'errors'=>$errors])?>
                </div>
                <div class="col-md-6 col-sm-12">
                    <?= my_form_upload('copia_dni','',['label' => 'Copia DNI/NIE. <code>*</code><div class="small"> DNI, tarjeta de identidad de extranjero, visado de estudios o tarjeta de estudiante extranjero. Archivo en formato pdf.</div>' ,'class'=>'', 'errors'=>$errors])?>
                </div>
                <div class="col-md-6 col-sm-12">
                    <?= my_form_upload('justificante','',['label' => 'Justificante de Pago. <code>*</code><div class="small">Justificante de la transferencia realizada. Archivo en formato pdf.</div>' ,'class'=>'', 'errors'=>$errors])?>                    
                </div>
                <div class="col-md-6 col-sm-12">
                    <?= my_form_upload('exenciones','',['label' => 'Documentación acreditativa de exención.<div class="small"> Documentación acreditativa de exención contemplada en la convocatoria. Archivo en formato pdf.</div>' ,'class'=>'', 'errors'=>$errors])?>                    
                </div>
                <div class="col-12">
                    <code class="small"> * obligatorio</code>
                </div>
            </div>    
        </div>
    </div>    

    <?= form_submit('enviar', 'Guardar', ['class'=>"btn btn-primary"]) ?>
<?= form_close()?>    
    
<?= $this->endSection() ?>

<a href="<?= base_url('/')?>" class="navbar-brand align-items-center brand-link d-flex flex-row py-3">
    <img src="<?= base_url('assets/images/logo0.png')?>" alt="Logo Armony Restaurante" class="brand-image img-circle elevation-3" width="50px">
    <span class="brand-text font-weight-light m-1">Armony Restaurante</span>
</a>
<button class="navbar-toggler border-black" type="button" data-bs-toggle="collapse" data-bs-target="#navbar1">
      <span class="navbar-toggler-icon"></span>
    </button>

<div class="collapse navbar-collapse" id="navbar1">
    <ul class="nav navbar-nav">
        <li class="nav-item d-flex d-sm-inline-block">
            <a href="<?= site_url('/')?>" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-flex d-sm-inline-block">
            <a href="<?= site_url('/contacto')?>" class="nav-link">Contacto</a>
        </li>
        <li class="nav-item d-flex d-sm-inline-block">
            <a href="<?= site_url('/carta')?>" class="nav-link">Carta</a>
        </li>
        <li class="nav-item d-flex d-sm-inline-block">
            <a href="<?= site_url('/reserva/elegir_personas')?>" class="nav-link">Reserva</a>
        </li>
    </ul>
    <ul class="nav navbar-nav">
        <?php if (auth()->loggedIn() AND auth()->user()->inGroup('admin')): ?>
            <li class="nav-item d-flex d-sm-inline-block">
                <a href="<?= site_url('empleados')?>" class="nav-link">
                    Empleados
                </a>
            </li>
        <?php endif ?>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li class="nav-item d-flex flex-row">
                    <?php if (auth()->loggedIn()): ?>
                <img src="<?= base_url('assets/images/logo-chef2.png')?>" class="img-circle elevation-2" alt="User Image" width="30px"><a href="<?= base_url('logout')?>" class="d-block"><?=auth()->user()->username ?></a>
                    <?php else: ?>
                        <a href="<?= base_url('login')?>" class="d-block">Autenticar</a>
                    <?php endif ?>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
    </ul>
</div>
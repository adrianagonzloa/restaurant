<?php 
/*******************************************************************************
* Carta -> entrantes
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla_public') ?>
<?= $this->section('content') ?>
<div class="mt-3" style="font-family: 'Dancing Script', cursive; font-size: 17px;">
    <div class="mb-5 text-center" id="home">
        <h2> Estos son nuestros entrantes </h2>
    </div>
    <!-- Va a ser responsivo, cada información de un plato es una linea -->
    <div class="d-flex justify-content-around flex-wrap">
        <!-- Info del primer plato -->
        <div class="row">
            <!-- En todos los apartados va a ser igual el reparto -->
            <!-- Se recoje la información de la base de datos, nombre, descripcion, precio -->
            <?php foreach($entrantes as $entrante): ?>
            <?php if ($entrante->cod_plato == 'PL101'): ?>
            <div class="col row text-center align-self-center border border-dark rounded-4 mx-2 p-2">
                <h1 class="mt-1" style="font-size: 30px;"><?= $entrante->nombre ?></h1>
                <div class="col align-self-center border-right border-dark mr-2">
                    <p><?= $entrante->descripcion ?></p>
                </div>
                <div class="col align-self-center">
                    <p><?= $entrante->coste_plato ?>€</p>
                </div>
            </div>
            <?php endif; ?>
            <?php endforeach; ?>

            <div class="mx-3 my-3 align-self-center col text-center">
                <img src="<?= base_url('assets/images/entrante.jpeg')?>" alt="Plato de entrante" width="300" height="200">
            </div>
        </div>
        <!-- Info del segundo plato -->
        <div class="row my-5">
            <?php foreach($entrantes as $entrante): ?>
            <?php if ($entrante->cod_plato == 'PL102'): ?>
            <div class="col row text-center align-self-center border border-dark rounded-4 mx-2 p-2">
                <h1 class="mt-1" style="font-size: 30px;"><?= $entrante->nombre ?></h1>
                <div class="col align-self-center border-right border-dark mr-2">
                    <p><?= $entrante->descripcion ?></p>
                </div>
                <div class="col align-self-center">
                    <p><?= $entrante->coste_plato ?>€</p>
                </div>
            </div>

            <div class="mx-3 my-3 align-self-center col text-center">
                <img src="<?= base_url('assets/images/entrante2.jpeg')?>" alt="Plato de entrante" width="300" height="200">
            </div>
            <?php endif; ?>
            <?php endforeach; ?>
       </div>
    </div>
    <!-- Boton para llegar otra vez a la carta -->
    <div class="my-4 text-center">    
        <a href="<?= base_url('carta')?>">
            <button class="btn btn-info" type="button" style="font-size: 19px"> Volver a la carta </button>
        </a>
    </div>
</div>
<?= $this->endSection() ?>


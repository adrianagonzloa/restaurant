<?php 
/*******************************************************************************
* Carta 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla_public') ?>


<?= $this->section('content') ?>
    
            
<div class="mt-3" style="font-family: 'Dancing Script', cursive; font-size: 17px;">
    <div class="mb-5 text-center" id="home">
        <h2> Disfruta de nuestra deliciosa carta </h2>
    </div>
    <div class="container">
        <div class="row my-5 d-flex justify-content-around flex-wrap">
            <!-- Entrantes -->
            <div class="col-12 col-md-6 d-flex justify-content-center">
                <div id="platos" class="text-center border align-self-center border-dark rounded-4 p-4" style="width: 100%; max-width: 400px;">
                    <h1 class="mt-3" style="font-size: 40px;">Entrantes</h1>
                    <dl>
                        <dd>
                            <p>Ensalada fresca de la huerta</p>
                        </dd>
                        <dd>
                            <p>Sopa de Verduras de Temporada</p>
                        </dd>
                    </dl>
                    <!-- Boton a informacion de esta parte de la carta -->
                    <div class="mb-3">
                        <a href="<?= base_url('carta/entrantes')?>">
                            <button class="btn btn-info" type="button">Ver con detalle</button>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Foto de entrante -->
            <div class="col mx-3 my-5 align-self-center text-center">
                <img src="<?= base_url('assets/images/entrante.jpeg')?>" alt="Plato de entrante" width="300" height="200">
            </div>
        </div>        
        
        <div class="row my-5 d-flex justify-content-around flex-wrap">
            <!-- Principal -->
            <div class="col-12 col-md-6 d-flex justify-content-center">
                <div id="platos" class="text-center border align-self-center border-dark rounded-4 p-4" style="width: 100%; max-width: 400px;">
                    <h1 class="mt-3" style="font-size: 40px;">Principal</h1>
                    <dl>
                        <dd>
                            <p>
                                Pollo a la parrilla con Hierbas Frescas y puré de patatas
                            </p>
                        </dd>
                        <dd>
                            <p>
                                Pasta con Salsa de Tomate y Albóndigas
                            </p>
                        </dd>
                        <dd>
                            <p>
                                Salmón al Horno con Ensalada de Quinoa
                            </p>
                        </dd>
                        <dd>
                            <p>
                                Patatas Asadas con Romero y Zanahorias Glaseadas
                            </p>
                        </dd>
                        <dd>
                            <p>
                            Pasta con Espinacas, Salsa de Queso y Champiñones
                            </p>
                        </dd>
                    </dl>
                    <!-- Boton a informacion de esta parte de la carta -->
                    <div class="mb-3">
                        <a href="<?= base_url('carta/principal')?>">
                          <button class="btn btn-info" type="button"> Ver con detalle </button>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Foto de Principal -->
            <div class="col my-5 mx-3 align-self-center text-center">
                <img src="<?= base_url('assets/images/principal.jpeg')?>" alt="Plato de Principal" width="300" height="200">
            </div>
        </div>
        
        <div class="row my-5 d-flex justify-content-around flex-wrap">
            <!-- Entrantes -->
            <div class="col-12 col-md-6 d-flex justify-content-center">
                <div id="platos" class="text-center border align-self-center border-dark rounded-4 p-4" style="width: 100%; max-width: 400px;">
                    <h1 class="mt-3" style="font-size: 40px;">Postres</h1>
                    <dl>
                        <dd>
                            <p>
                                Frutas Frescas con Yogur Natural
                            </p>
                        </dd>
                        <dd>
                            <p>
                                Tarta de Manzana Casera
                            </p>
                        </dd>
                    </dl>
                    <!-- Boton a informacion de esta parte de la carta -->
                    <div class="mb-3">    
                        <a href="<?= base_url('carta/postres')?>">
                            <button class="btn btn-info" type="button"> Ver con detalle </button>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Foto de Principal -->
            <div class="col my-5 mx-3 align-self-center text-center">
                <img src="<?= base_url('assets/images/postre.jpeg')?>" alt="Plato de Postre" width="300" height="200">
            </div>
        </div>
        
        <div class="row my-5 d-flex justify-content-around flex-wrap">
            <!-- Bebida -->
            <div class="col-12 col-md-6 d-flex justify-content-center">
                <div id="platos" class="text-center border align-self-center border-dark rounded-4 p-4" style="width: 100%; max-width: 400px;">
                    <h1 class="mt-3" style="font-size: 40px;">Bebida</h1>
                    <dl>
                        <dd>
                            <p>
                                Limonada Refrescante
                            </p>
                        </dd>
                        <dd>
                            <p>
                                Té Verde
                            </p>
                        </dd>
                        <dd>
                            <p>
                            Café Americano
                            </p>
                        </dd>
                        <dd>
                            <p>
                                Café espreso
                            </p>
                        </dd>
                        <dd>
                            <p>
                                Café con Leche
                            </p>
                        </dd>
                        <dd>
                            <p>
                                Botella de Agua de 500 mL 
                            </p>
                        </dd>
                    </dl>
                    <!-- Boton a informacion de esta parte de la carta -->
                    <div class="mb-4">    
                        <a href="<?= base_url('carta/bebida')?>">
                            <button class="btn btn-info" type="button"> Ver con detalle </button>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Foto de Principal -->
            <div class="col my-5 mx-3 align-self-center text-center">
                <img src="<?= base_url('assets/images/bebida.jpeg')?>" alt="Bebidas" width="300" height="200">
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>



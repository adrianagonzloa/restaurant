<?php 
/*******************************************************************************
 * Vista con la página inicial/principal
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla_public') ?>

<?= $this->section('content') ?><!-- Contenido de la página principal -->
    <div class="text-center text-black" id="home">
        <div class="mb-5">
            <h1 style="color: #009c8c; font-size: 50px"><strong> Bienvenido a Armony Restaurante </strong></h1>
            <p style="font-size: 20px; color: black;" class="mb-4"> En este restaurante vas a tener una experiencia única con las creaciones culinarias de nuestro chef</p>
            <img class="img-fluid" src="<?= base_url('assets/images/restaurante.jpg')?>" alt="Plato de comida" width="1150" height="1100">
        </div>  
        <div class="pt-5">
            <h2 style="color: #009c8c; font-size: 40px">Descubre nuestra carta</h2><!-- color: #ADD8E6" -->
            <div class="my-5">
                <a href="<?= base_url('carta')?>" target="_blank">
                   <button class="btn p-3" style="background-color: #87CEEB; font-size: 20px;" type="button"> Ver Carta </button>
                </a>
            </div>
        </div>
        <div class="m-5 pt-5">
            <h3 style="color: #009c8c; font-size: 40px">Horario</h3>
            <p style="font-size: 20px;">Lunes a Viernes: De 12 a 16 y de 19 a 23</p>
            <p style="font-size: 20px;">Sabado a Domingo: De 12 a 17</p>
        </div>
        <div class="m-5 pt-5">
            <h3 style="color: #009c8c;">Siguenos en nuestras redes sociales</h3>
            <a href="#"><img src="<?= base_url('assets/images/facebook.png')?>" alt="Facebook" width="35" height="35"></a>
            <a href="#"><img src="<?= base_url('assets/images/twiter.png')?>" alt="Twitter" width="40" height="35"></a>
            <a href="#"><img src="<?= base_url('assets/images/insta.png')?>" alt="Instagram" width="35" height="35"></a>
        </div>
    </div>
<?= $this->endSection() ?>


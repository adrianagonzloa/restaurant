<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>



<?= $this->section('content') ?><!-- Contenido de la página principal -->
    <div class="text-center" id="home">
        <div class="mb-5">
            <h1 class="text-info"><strong> Bienvenido a Armony Restaurante </strong></h1>
            <p> En este restaurante vas a tener una experiencia única con las creaciones culinarias de nuestro chef</p>
            <img src="<?= base_url('assets/images/rest.jpeg')?>" alt="Plato de comida" width="300" height="200">
        </div>  
        <div>
            <h2>Descubre nuestra carta</h2>
            <div class="mb-4">
                <a href="<?= base_url('carta')?>" target="_blank">
                   <button class="btn btn-info p-3" type="button"> Ver Carta </button>
                </a>
            </div>
        </div>
        <div class="m-5">
            <h3>Horario</h3>
            <p>Lunes a Viernes: De 12 a 16 y de 19 a 23</p>
            <p>Sabado a Domingo: De 12 a 17</p>
        </div>
        <div>
            <h3>Contactanos</h3>
            <p>Dirección: C/Ángel Villena, s/n. 46013 Valencia</p>
            <p>Telefono: 123456789</p>
            <p>Email: adrianag200404@gmail.com</p>
        </div>
        <div class="m-5">
            <h4>Siguenos en nuestras redes sociales</h4>
            <a href="#"><img src="<?= base_url('assets/images/facebook.png')?>" alt="Facebook" width="35" height="35"></a>
            <a href="#"><img src="<?= base_url('assets/images/twiter.png')?>" alt="Twitter" width="40" height="35"></a>
            <a href="#"><img src="<?= base_url('assets/images/insta.png')?>" alt="Instagram" width="35" height="35"></a>
        </div>
            
    </div>
<?= $this->endSection() ?>


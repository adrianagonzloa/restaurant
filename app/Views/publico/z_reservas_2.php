<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>
<!-- css y js para datapicker -->
<?= $this->section('css') ?>
    <?= $this->include('common/fechahora_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/fechahora_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<!-- Es como prueba para reservar, todavía no es funcional -->
    <?= form_open('#')?>
        <!-- Aparece para escojer la hora y día -->
        <div style="overflow:hidden;">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-8">
                        <div id="datetimepicker13"></div>
                    </div>
                </div>
            </div>

        </div>
        <?= form_submit('Enviar','Enviar',['class'=>'btn btn-danger'])?>
    <?= form_close('')?>
    
<?= $this->endSection() ?>


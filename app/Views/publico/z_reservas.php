<?php 
/*******************************************************************************
 * Ejemplo de reservas
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>
<!-- css y js para datapicker -->
<?= $this->section('css') ?>
    <?= $this->include('common/fechahora_css') ?>
<!-- Da color al botón de AM/PM, cambia la letra de toda la página -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.0.0/flatly/bootstrap.min.css">
<!-- Las flechas son más gruesas -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">



<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/fechahora_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?= form_open('#') ?>
        <div style="overflow:hidden;">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-8">
                        <div id="datetimepicker13"></div>
                    </div>
                </div>
            </div>
            <?= form_submit('Enviar','Enviar',['class'=>'btn btn-danger']) ?>
        </div>
    <?= form_close() ?>

    <!-- Aquí muestra la fecha seleccionada -->
    <div id="fecha_seleccionada"></div>
    
<?= $this->endSection() ?>
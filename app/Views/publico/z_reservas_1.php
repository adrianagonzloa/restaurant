<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>
<!-- css y js para datapicker -->
<?= $this->section('css') ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://resources/demos/style.css">
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datepicker') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    <div class="row">
        <div class="form-group">
            <div class="input-group date" id="datetimepicker1">
                <label for="datetimepicker1" class="pt-2 pr-2">日付:</label>
                <input type="text" class="form-control" />
                <span class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <div class="input-group date" id="datetimepicker2">
                <label for="datetimepicker2" class="pt-2 pr-2">時間:</label>
                <input type="text" class="form-control" />
                <span class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                </span>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            dayViewHeaderFormat: 'YYYY年 MMMM',
            tooltips: {
                close: '閉じる',
                selectMonth: '月を選択',
                prevMonth: '前月',
                nextMonth: '次月',
                selectYear: '年を選択',
                prevYear: '前年',
                nextYear: '次年',
                selectTime: '時間を選択',
                selectDate: '日付を選択',
                prevDecade: '前期間',
                nextDecade: '次期間',
                selectDecade: '期間を選択',
                prevCentury: '前世紀',
                nextCentury: '次世紀'
            },
            format: 'YYYY/MM/DD',
            locale: 'ja',
            showClose: true
        });
        $('#datetimepicker2').datetimepicker({
            tooltips: {
                close: '閉じる',
                pickHour: '時間を取得',
                incrementHour: '時間を増加',
                decrementHour: '時間を減少',
                pickMinute: '分を取得',
                incrementMinute: '分を増加',
                decrementMinute: '分を減少',
                pickSecond: '秒を取得',
                incrementSecond: '秒を増加',
                decrementSecond: '秒を減少',
                togglePeriod: '午前/午後切替',
                selectTime: '時間を選択'
            },
            format: 'HH:mm',
            locale: 'ja',
            showClose: true
        });
    });
</script>
    
    
    
    
    
    
    
    
    
<?= $this->endSection() ?>


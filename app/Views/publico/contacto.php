<?php 
/*******************************************************************************
 * CONTACTO
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla_public') ?>

<?= $this->section('content') ?>
    <div class="text-center">
        <div>
            <h3 style="color: #009c8c; font-size: 40px">Contactanos</h3>
            <p style="font-size: 20px;">Dirección: C/Ángel Villena, s/n. 46013 Valencia</p>
            <p style="font-size: 20px;">Telefono: 123456789</p>
            <p style="font-size: 20px;">Email: adrianag200404@gmail.com</p>
        </div>
        <div class="m-5">
            <h3 style="color: #009c8c;">Siguenos en nuestras redes sociales</h3>
            <a href="#"><img src="<?= base_url('assets/images/facebook.png')?>" alt="Facebook" width="35" height="35"></a>
            <a href="#"><img src="<?= base_url('assets/images/twiter.png')?>" alt="Twitter" width="40" height="35"></a>
            <a href="#"><img src="<?= base_url('assets/images/insta.png')?>" alt="Instagram" width="35" height="35"></a>
        </div>
    </div>
<?= $this->endSection() ?>


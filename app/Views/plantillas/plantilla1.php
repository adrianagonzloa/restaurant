<!DOCTYPE html>
<html lang="en" style="height: auto;">
<!--            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Armony Restaurante</title>
        <!-- favicon -->
        <link rel="icon" href="<?= base_url('assets/images/logo0.png') ?>">
        <!-- iconos de bootstrap -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
        
        <!-- Fuentes de google -->
            <!--        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>-->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Dancing+Script&display=swap" rel="stylesheet">
        <!-- CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome/all.min.css')?>">
        <link rel="stylesheet" href="<?= base_url('assets/css/adminlte.min.css')?>">
        <link rel="stylesheet" href="<?= base_url('assets/css/restaurante.css')?>"><!-- css propio de mi web -->
        <!-- css ocasional -->
        <?= $this->renderSection('css') ?>
    </head>
    
    <body class="sidebar-mini" style="height: auto;">

        <div class="wrapper">
            <!-- navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <?= $this->include('common/navbar') ?><!-- Navbar -->
            </nav>
            
            <aside class="main-sidebar sidebar-dark-primary elevation-4" 
                   style="position:fixed;"> 
                <?= $this->include('common/sidebar') ?><!-- Sidebar -->
            </aside>

            <div class="content-wrapper" style="min-height: 1604.8px;">

                <section class="content-header">
                    <div class="container-fluid">
                        <h1><?= $this->renderSection('page_title') ?></h1>
                    </div>
                </section>
                    
                <section class="content">
                        <div class="mx-4 p-3 rounded-4"> 
                           <?= $this->renderSection('content') ?><!-- Aquí se pone el contenido de las otras vistas -->
                        </div>                                           
                </section>

            </div>

            <footer class="main-footer">
                <?= $this->include('common/footer') ?><!-- Footer, pie de página -->
            </footer>

        <script src="<?= base_url('assets/js/jquery.min.js')?>"></script>
        <script src="<?= base_url('assets/js/adminlte.min.js')?>"></script>
        <!-- java ocasional -->
        <?= $this->renderSection('js') ?>
    </body>
</html>


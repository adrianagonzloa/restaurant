<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
    	
        <h2><?= $this->renderSection('title')?></h2>
<!-- Esta parte será para cuando ponga la validacion -->
        <?php if (!empty($errores)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errores as $field => $error): ?>
                    <p><?= $field.' - '.$error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
<!-- Formulario para el cliente -->
        <?= form_open('proveedores/editar/'.$proveedor->cod_proveedor) ?>
		<div class="form-group">
                    <?= form_label('Nombre:','nombre')?>
                    <?= form_input('nombre',$proveedor->nombre,['cod_proveedor'=>'nombre','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Dirección:','direccion')?>
                    <?= form_input('direccion',$proveedor->direccion,['cod_proveedor'=>'direccion','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Ciudad:','ciudad')?>
                    <?= form_input('ciudad',$proveedor->ciudad,['cod_proveedor'=>'ciudad','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Teléfono:','telefono')?>
                    <?= form_input('telefono',$proveedor->telefono,['cod_proveedor'=>'telefono','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('E-mail:','correo_electronico')?>
                    <?= form_input('correo_electronico',$proveedor->correo_electronico,['cod_proveedor'=>'correo_electronico','class'=>'form-control']) ?>
		</div>


            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>
    </div>
    

<?= $this->endSection() ?>

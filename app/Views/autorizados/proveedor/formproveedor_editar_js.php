<script src="<?= base_url('assets/js/jquery.validate.min.js') ?>"></script>
<script src="<?= base_url('assets/js/additional-methods.min.js') ?>"></script>

<script src="<?= base_url('assets/js/localization/messages_es.js') ?>"></script>
<script>
    $(document).ready(function () {
        
        // Aquí se comienza especificar la validación del formulario
        $("#proveedor_editar").validate({
            errorClass: "error is-invalid",
            validClass: "valid is-valid",

            // Reglas
            rules: {
                nombre: {
                    required: true,
                    minlength: 3,
                    maxlength: 100
                },
                direccion:{
                    maxlength: 200
                }, 
                ciudad:{
                    maxlength: 200
                }, 
                telefono: {
                    required: true,
                    digits: true,
                    exactlength: 9
                },
                correo_electronico: {
                     required: true,
                     email: true
                }
            },
            
            errorPlacement: function(error, element) {
                if (element.is("input[type='file']")){
                    error.appendTo(element.parent("div").parent("div").parent("div"));
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
       });
    });
</script>

<?php 
/*******************************************************************************
 * Tabla de los proveedores
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Ciudad</th>
                    <th>Teléfono</th>
                    <th>E-mail</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($proveedores as $proveedor): ?> 
                    <tr>
                        <td><?= $proveedor->cod_proveedor ?></td>
                        <td><?= $proveedor->nombre ?></td>
                        <td><?= $proveedor->direccion ?></td>
                        <td><?= $proveedor->ciudad ?></td>
                        <td><?= $proveedor->telefono ?></td>
                        <td><?= $proveedor->correo_electronico ?></td>
                        <td>
                            <a href="<?= site_url('proveedores/editar/'.$proveedor->cod_proveedor)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('proveedores/borrar/'.$proveedor->cod_proveedor)?>" onclick="return confirm('¿Estás seguro de que quieres borrar el proveedor seleccionado?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

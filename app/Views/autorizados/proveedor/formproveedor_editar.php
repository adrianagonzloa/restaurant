<?php 
/*******************************************************************************
 * Formulario de edición del proveedor
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/proveedor/formproveedor_editar_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para el proveedor -->

        <?= form_open_multipart('proveedores/editar/'.$proveedor->cod_proveedor,['id'=>'proveedor_editar']) ?> <!-- , ['id'=>'solicitud'] / form_open_multipart para añadir atributos, por ejemplo, subir archivos con ese formulario -->
            <div class="col-lg-9">
                <?= my_form_input('nombre',$proveedor->nombre,['label' => 'Nombre: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('direccion',$proveedor->direccion,['label'=>'Dirección: ' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9 ">
                <?= my_form_input('ciudad',$proveedor->ciudad,['label'=>'Ciudad: ' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class=" col-lg-3">
                <?= my_form_input('telefono',$proveedor->telefono,['label' => 'Teléfono: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('correo_electronico',$proveedor->correo_electronico,['label' => 'Email: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>

            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>

<?= $this->endSection() ?>

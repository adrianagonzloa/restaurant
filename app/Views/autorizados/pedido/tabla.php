<?php 
/*******************************************************************************
 * Tabla de pedido
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>Código Pedido</th>
                    <th>Código del Proveedor</th>
                    <th>Fecha</th>
                    <th>Estado</th>
                    <th>Coste</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($pedidos as $pedido): ?> 
                    <tr>
                        <td><?= $pedido->cod_pedido?></td>
                        <td><?= $pedido->cod_proveedor ?></td>
                        <td><?= $pedido->fecha ?></td>
                        <td><?= $pedido->estado ?></td>
                        <td><?= $pedido->coste ?></td>
                        <td>
                            <a href="<?= site_url('pedidos/editar/'.$pedido->cod_pedido)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('pedidos/borrar/'.$pedido->cod_pedido)?>" onclick="return confirm('¿Estás seguro de que quieres borrar el pedido seleccionado?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

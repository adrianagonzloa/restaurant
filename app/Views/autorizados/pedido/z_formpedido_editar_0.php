<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
<!-- Esta parte será para cuando ponga la validacion -->
        <h2><?= $this->renderSection('title')?></h2>

        <?php if (!empty($errores)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errores as $field => $error): ?>
                    <p><?= $field.' - '.$error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
<!-- Formulario para el cliente -->
        <?= form_open('pedidos/editar/'.$pedido->cod_pedido) ?>
		<div class="form-group">
                    <?= form_label('Código de proveedor:','cod_proveedor')?>
                    <?= form_input('cod_proveedor',$pedido->cod_proveedor,['cod_pedido'=>'cod_proveedor','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Fecha:','fecha')?>
                    <?= form_input('fecha',$pedido->fecha,['cod_pedido'=>'fecha','class'=>'form-control']) ?>
        	</div>
                <div class="form-group">
                    <?= form_label('Estado:','estado')?>
                    <?= form_input('estado',$pedido->estado,['cod_pedido'=>'estado','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Coste:','coste')?>
                    <?= form_input('coste',$pedido->coste,['cod_pedido'=>'coste','class'=>'form-control']) ?>
		</div>


            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>
    </div>
    

<?= $this->endSection() ?>

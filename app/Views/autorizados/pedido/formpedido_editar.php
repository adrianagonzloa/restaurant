<?php 
/*******************************************************************************
 * Formulario de edición de los pedidos
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/pedido/formpedido_editar_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para el pedido -->

        <?= form_open_multipart('pedidos/editar/'.$pedido->cod_pedido,['id'=>'pedido_editar']) ?> <!-- , ['id'=>'solicitud'] / form_open_multipart para añadir atributos, por ejemplo, subir archivos con ese formulario -->
            <div class="col-lg-9">
                <?= my_form_input('cod_proveedor',$pedido->cod_proveedor,['label' => 'Código de proveedor:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly' => true])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('fecha',$pedido->fecha,['label' => 'Fecha: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= form_label('Estado:  <code>*</code>','estado')?>
                <?= form_dropdown('estado', ['Recibido' => 'Recibido', 'Cancelado' => 'Cancelado', 'En Proceso' => 'En Proceso'], $pedido->estado, 'class="form-control"', 'errors=$errors') ?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('coste',$pedido->coste,['label'=>'Coste: ' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
        
            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>
<?= $this->endSection() ?>

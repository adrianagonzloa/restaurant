<?php 
/*******************************************************************************
 * Formulario de edición de reserva de mesas
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/reserva_mesa/formreservamesa_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para la reserva de las mesas -->

        <?= form_open_multipart('reserva_mesa/editar/'.$reserva->id,['id'=>'reservamesa']) ?> 
            <div class="col-lg-9">
                <?= my_form_input('cod_cliente',$reserva->cod_cliente,['label' => 'Código del pedido del cliente:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly' => true])?>
            </div>

            <div class="col-lg-9">
                <?= my_form_input('cod_mesa',$reserva->cod_mesa,['label' => 'Código de la mesa:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly' => true])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('fecha',$reserva->fecha,['label' => 'Fecha: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('cod_turno',$reserva->cod_turno,['label' => 'Código del turno: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('cantidad_personas',$reserva->cantidad_personas !== null ? $reserva->cantidad_personas : '',['label'=>'Cantidad de personas: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9 ">
                <?= my_form_input('estado',$reserva->estado,['label'=>'Estado: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>

            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>


    

<?= $this->endSection() ?>

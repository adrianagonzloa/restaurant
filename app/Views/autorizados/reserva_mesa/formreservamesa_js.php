<script src="<?= base_url('assets/js/jquery.validate.min.js') ?>"></script>
<script src="<?= base_url('assets/js/additional-methods.min.js') ?>"></script>

<script src="<?= base_url('assets/js/localization/messages_es.js') ?>"></script>
<script>
    $(document).ready(function () {
        
        // Aquí se comienza especificar la validación del formulario
        $("#reservamesa").validate({
            errorClass: "error is-invalid",
            validClass: "valid is-valid",

            // Reglas
            rules: {
                cod_cliente: {
                    required: true  
                },    
                cod_mesa: {
                    required: true   
                },     
                fecha: {
                    required: true,
                    date: true
                },  
                cod_turno: {
                    required: true   
                },
                cantidad_personas: {
                    //required: true,
                    digits: true,
                    maxlength: 2,
                    minlength: 1
                }//,
//                estado:{
//                    maxlength: 10
//                }
            },
            
            errorPlacement: function(error, element) {
                if (element.is("input[type='file']")){
                    error.appendTo(element.parent("div").parent("div").parent("div"));
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>

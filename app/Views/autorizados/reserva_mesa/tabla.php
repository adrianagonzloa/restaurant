<?php 
/*******************************************************************************
 * Tabla de las reservas de las mesas
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Reserva</th>
                    <th>Código Cliente</th>
                    <th>Código Mesa</th>
                    <th>Fecha</th>
                    <th>Turno</th>
                    <th>Personas</th>
                    <th>Estado</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($reserva_mesas as $reserva_mesa): ?> 
                    <tr>
                        <td><?= $reserva_mesa->id ?></td>
                        <td><?= $reserva_mesa->cod_reserva ?></td>
                        <td><?= $reserva_mesa->cod_cliente ?></td>
                        <td><?= $reserva_mesa->cod_mesa ?></td>
                        <td><?= $reserva_mesa->fecha ?></td>
                        <td><?= $reserva_mesa->cod_turno ?></td>
                        <td><?= $reserva_mesa->cantidad_personas ?></td>
                        <td><?= $reserva_mesa->estado ?></td>
                        <td>
                            <a href="<?= site_url('reserva_mesa/editar/'.$reserva_mesa->id)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('reserva_mesa/borrar/'.$reserva_mesa->id)?>" onclick="return confirm('¿Estás seguro de que quieres borrar la reserva seleccionada?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
    	
        <h2><?= $this->renderSection('title')?></h2>
<!-- Esta parte será para cuando ponga la validacion -->
        <?php if (!empty($errores)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errores as $field => $error): ?>
                    <p><?= $field.' - '.$error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
<!-- Formulario para el cliente -->
        <?= form_open('reserva_mesa/editar/'.$reserva->cod_reserva) ?>
		<div class="form-group">
                    <?= form_label('Código del cliente:','cod_cliente')?>
                    <?= form_input('cod_cliente',$reserva->cod_cliente,['cod_reserva'=>'cod_cliente','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Código de la mesa:','cod_mesa')?>
                    <?= form_input('cod_mesa',$reserva->cod_mesa,['cod_reserva'=>'cod_mesa','class'=>'form-control']) ?>
        	</div>
                <div class="form-group">
                    <?= form_label('Fecha y Hora:','fecha_hora')?>
                    <?= form_input('fecha_hora',$reserva->fecha_hora,['cod_reserva'=>'fecha_hora','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Duración de la reserva (90):','duracion_reserva')?>
                    <?= form_input('duracion_reserva',$reserva->duracion_reserva,['cod_reserva'=>'duracion_reserva','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Cantidad de personas:','cantidad_personas')?>
                    <?= form_input('cantidad_personas',$reserva->cantidad_personas,['cod_reserva'=>'cantidad_personas','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Estado:','estado')?>
                    <?= form_input('estado',$reserva->estado,['cod_reserva'=>'estado','class'=>'form-control']) ?>
		</div>


            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>
    </div>
    

<?= $this->endSection() ?>

<?php 
/*******************************************************************************
 * Formulario de edición de plato
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/plato/formplato_editar_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para el plato -->

        <?= form_open_multipart('platos/editar/'.$plato->cod_plato,['id'=>'plato_editar']) ?> <!-- , ['id'=>'solicitud'] / form_open_multipart para añadir atributos, por ejemplo, subir archivos con ese formulario -->
            <div class="col-lg-9">
                <?= my_form_input('nombre',$plato->nombre,['label' => 'Nombre:' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('coste_plato',$plato->coste_plato,['label' => 'Coste del plato: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9 ">
                <?= my_form_input('descripcion',$plato->descripcion !== null ? $plato->descripcion : '',['label'=>'Descripción: ' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>

            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>

<?= $this->endSection() ?>

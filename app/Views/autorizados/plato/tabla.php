<?php 
/*******************************************************************************
 * Tabla de los platos
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>Código Plato</th>
                    <th>Nombre</th>
                    <th>Coste_plato</th>
                    <th>Descripcion</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($platos as $plato): ?> 
                    <tr>
                        <td><?= $plato->cod_plato ?></td>
                        <td><?= $plato->nombre ?></td>
                        <td><?= $plato->coste_plato ?></td>
                        <td><?= $plato->descripcion ?></td>
                        <td>
                            <a href="<?= site_url('platos/editar/'.$plato->cod_plato)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('platos/borrar/'.$plato->cod_plato)?>" onclick="return confirm('¿Estás seguro de que quieres borrar el plato seleccionado?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

<script src="<?= base_url('assets/js/jquery.validate.min.js') ?>"></script>
<script src="<?= base_url('assets/js/additional-methods.min.js') ?>"></script>
<script src="<?= base_url('assets/js/localization/messages_es.js') ?>"></script>
<script>
    $(document).ready(function () {
        
        // Aquí se comienza especificar la validación del formulario
        $("#plato_editar").validate({
            errorClass: "error is-invalid",
            validClass: "valid is-valid",

            // Reglas
            rules: {
                nombre: {
                    required: true,
                    maxlength: 100
                },
                coste_plato: {
                    required: true,
                    number: true
                }, 
                descripcion:{
                    maxlength: 250
                }
            },
            
            errorPlacement: function(error, element) {
                if (element.is("input[type='file']")){
                    error.appendTo(element.parent("div").parent("div").parent("div"));
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>

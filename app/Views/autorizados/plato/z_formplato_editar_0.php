<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
    	
        <h2><?= $this->renderSection('title')?></h2>

        <?php if (!empty($errores)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errores as $field => $error): ?>
                    <p><?= $field.' - '.$error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>

        <?= form_open('platos/editar/'.$plato->cod_plato) ?>
		<div class="form-group">
                    <?= form_label('Nombre:','nombre')?>
                    <?= form_input('nombre',$plato->nombre,['cod_plato'=>'nombre','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Coste del plato:','coste_plato')?>
                    <?= form_input('coste_plato',$plato->coste_plato,['cod_plato'=>'coste_plato','class'=>'form-control']) ?>
        	</div>
                <div class="form-group">
                    <?= form_label('Descripcion:','descripcion')?>
                    <?= form_input('descripcion',$plato->descripcion,['cod_plato'=>'descripcion','class'=>'form-control']) ?>
		</div>


            <!--<button type="submit" class="btn btn-primary">Guardar</button>-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>
            <!--</form>-->
    </div>
    

<?= $this->endSection() ?>

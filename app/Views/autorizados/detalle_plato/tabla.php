<?php 
/*******************************************************************************
 * Tabla de detalle plato
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>id</th>
                    <th>Código Plato</th>
                    <th>Código del Producto</th>
                    <th>Cantidad Producto</th>
                    <th>Descripcion</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($detalle_platos as $detalle_plato): ?> 
                    <tr>
                        <td><?= $detalle_plato->id ?></td>
                        <td><?= $detalle_plato->cod_plato ?></td>
                        <td><?= $detalle_plato->cod_producto ?></td>
                        <td><?= $detalle_plato->cantidad_producto ?></td>
                        <td><?= $detalle_plato->descripcion ?></td>
                        <td>
                            <a href="<?= site_url('detalle_plato/editar/'.$detalle_plato->id)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('detalle_plato/borrar/'.$detalle_plato->id)?>" onclick="return confirm('¿Estás seguro de que quieres borrar el detalle del plato seleccionado?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

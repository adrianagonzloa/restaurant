<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
<!-- Esta parte será para cuando ponga la validacion -->
        <h2><?= $this->renderSection('title')?></h2>

        <?php if (!empty($errores)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errores as $field => $error): ?>
                    <p><?= $field.' - '.$error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
<!-- Formulario para el cliente -->
        <?= form_open('detalle_plato/editar/'.$detalle_plato->id) ?>
		<div class="form-group">
                    <?= form_label('Código del Plato:','cod_plato')?>
                    <?= form_input('cod_plato',$detalle_plato->cod_plato,['id'=>'cod_plato','class'=>'form-control', 'readonly' => true]) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Código del producto:','cod_producto')?>
                    <?= form_input('cod_producto',$detalle_plato->cod_producto,['id'=>'cod_producto','class'=>'form-control', 'readonly' => true]) ?>
        	</div>
                <div class="form-group">
                    <?= form_label('Cantidad Producto:','cantidad_producto')?>
                    <?= form_input('cantidad_producto',$detalle_plato->cantidad_producto,['id'=>'cantidad_producto','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Descripción:','descripcion')?>
                    <?= form_input('descripcion',$detalle_plato->descripcion  !== null ? $detalle_plato->descripcion : '',['id'=>'descripcion','class'=>'form-control']) ?>
                            <!-- Pasa lo mismo que con Consumicion, es nula,['cod_cliente'=>'ciudad','class'=>'form-control']) ?>-->
		</div>


            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>
    </div>
    

<?= $this->endSection() ?>

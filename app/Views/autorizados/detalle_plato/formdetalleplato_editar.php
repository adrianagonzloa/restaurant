<?php 
/*******************************************************************************
 * Formulario de edición de detalle_plato
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/detalle_plato/formdetalleplato_editar_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para detalle_plato -->

        <?= form_open_multipart('detalle_plato/editar/'.$detalle_plato->id,['id'=>'detalleplato_editar']) ?> <!-- , ['id'=>'solicitud'] / form_open_multipart para añadir atributos, por ejemplo, subir archivos con ese formulario -->
            <div class="col-lg-9">
                <?= my_form_input('cod_plato',$detalle_plato->cod_plato,['label' => 'Código del Plato:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly' => true])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('cod_producto',$detalle_plato->cod_producto,['label' => 'Código del producto:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly' => true])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('cantidad_producto',$detalle_plato->cantidad_producto,['label' => 'Cantidad Producto: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9 ">
                <?= my_form_input('descripcion',$detalle_plato->descripcion  !== null ? $detalle_plato->descripcion : '',['label'=>'Descripción: ' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>
<?= $this->endSection() ?>

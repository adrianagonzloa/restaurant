<?php 
/*******************************************************************************
 * Formulario de edición del producto
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/producto/formproducto_editar_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para el producto -->

        <?= form_open_multipart('productos/editar/'.$producto->cod_producto,['id'=>'producto_editar']) ?> <!-- , ['id'=>'solicitud'] / form_open_multipart para añadir atributos, por ejemplo, subir archivos con ese formulario -->
            <div class="col-lg-9">
                <?= my_form_input('nombre',$producto->nombre,['label' => 'Nombre: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('categoria',$producto->categoria,['label' => 'Categoria: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('unidades_stock',$producto->unidades_stock,['label' => 'Unidades en Stock: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('presentacion',$producto->presentacion,['label' => 'Presentación: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('razon',$producto->razon,['label' => 'Razón: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('precio',$producto->precio,['label'=>'Precio: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('stock_minimo',$producto->stock_minimo,['label'=>'Stock Mínimo: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9 ">
                <?= my_form_input('pedido',$producto->pedido !== null ? $producto->pedido: '',['label'=>'Pedido: ' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('stock',$producto->stock,['label'=>'Stock: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>

            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>

<?= $this->endSection() ?>

<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
    	
        <h2><?= $this->renderSection('title')?></h2>
<!-- Esta parte será para cuando ponga la validacion -->
        <?php if (!empty($errores)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errores as $field => $error): ?>
                    <p><?= $field.' - '.$error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
<!-- Formulario para el cliente -->
        <?= form_open('productos/editar/'.$producto->cod_producto) ?>
		<div class="form-group">
                    <?= form_label('Nombre:','nombre')?>
                    <?= form_input('nombre',$producto->nombre,['cod_producto'=>'nombre','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Categoria:','categoria')?>
                    <?= form_input('categoria',$producto->categoria,['cod_producto'=>'categoria','class'=>'form-control']) ?>
        	</div>
                <div class="form-group">
                    <?= form_label('Unidades en Stock:','unidades_stock')?>
                    <?= form_input('unidades_stock',$producto->unidades_stock,['cod_producto'=>'unidades_stock','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Presentación:','presentacion')?>
                    <?= form_input('presentacion',$producto->presentacion,['cod_producto'=>'presentacion','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Razon:','razon')?>
                    <?= form_input('razon',$producto->razon,['cod_producto'=>'razon','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Precio:','precio')?>
                    <?= form_input('precio',$producto->precio,['cod_producto'=>'precio','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Stock Mínimo:','stock_minimo')?>
                    <?= form_input('stock_minimo',$producto->stock_minimo,['cod_producto'=>'stock_minimo','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Pedido:','pedido')?>
                    <?= form_input('pedido',$producto->pedido !== null ? $producto->pedido : '',['cod_producto'=>'pedido','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Stock:','stock')?>
                    <?= form_input('stock',$producto->stock,['cod_producto'=>'stock','class'=>'form-control']) ?>
		</div>


            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>
    </div>
    

<?= $this->endSection() ?>

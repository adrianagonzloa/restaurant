<?php 
/*******************************************************************************
 * Tabla de los productos
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Categoria</th>
                    <th>Unidades en Stock</th>
                    <th>Presentación</th>
                    <th>Razón</th>
                    <th>Precio</th>
                    <th>Stock Mínimo</th>
                    <th>Pedido</th>
                    <th>Stock</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($productos as $producto): ?> 
                    <tr>
                        <td><?= $producto-> cod_producto ?></td>
                        <td><?= $producto->nombre ?></td>
                        <td><?= $producto->categoria ?></td>
                        <td><?= $producto->unidades_stock ?></td>
                        <td><?= $producto->presentacion ?></td>
                        <td><?= $producto->razon ?></td>
                        <td><?= $producto->precio ?></td>
                        <td><?= $producto->stock_minimo ?></td>
                        <td><?= $producto->pedido ?></td>
                        <td><?= $producto->stock ?></td>
                        <td>
                            <a href="<?= site_url('productos/editar/'.$producto->cod_producto)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('productos/borrar/'.$producto->cod_producto)?>" onclick="return confirm('¿Estás seguro de que quieres borrar el producto seleccionado?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

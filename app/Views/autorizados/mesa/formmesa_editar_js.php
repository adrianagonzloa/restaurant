<script src="<?= base_url('assets/js/jquery.validate.min.js') ?>"></script>
<script src="<?= base_url('assets/js/additional-methods.min.js') ?>"></script>

<script src="<?= base_url('assets/js/localization/messages_es.js') ?>"></script>
<script>
    $.validator.addMethod("exactlength", function(value, element, param) {
        return this.optional(element) || value.length == param;
    }, $.validator.format("Por favor entre un número de teléfono válido {0} dígitos"));
    
    $(document).ready(function () {
        
        // Aquí se comienza especificar la validación del formulario
        $("#mesa_editar").validate({
            errorClass: "error is-invalid",
            validClass: "valid is-valid",

            // Reglas
            rules: {
                capacidad: {
                    required: true,
                    digits: true
                },
                estado: {
                    required: true
                }
            },
            
            errorPlacement: function(error, element) {
                if (element.is("input[type='file']")){
                    error.appendTo(element.parent("div").parent("div").parent("div"));
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>

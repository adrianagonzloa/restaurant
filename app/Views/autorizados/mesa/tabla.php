<?php 
/*******************************************************************************
 * Tabla de las mesas
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>Código Mesa</th>
                    <th>Capacidad</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($mesas as $mesa): ?> 
                    <tr>
                        <td><?= $mesa->cod_mesa ?></td>
                        <td><?= $mesa->capacidad ?></td>
                        <td><?= $mesa->estado ?></td>
                        <td>
                            <a href="<?= site_url('mesas/editar/'.$mesa->cod_mesa)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('mesas/borrar/'.$mesa->cod_mesa)?>" onclick="return confirm('¿Estás seguro de que quieres borrar la mesa seleccionada?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

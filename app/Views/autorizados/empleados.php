<?php 
/*******************************************************************************
 * Vista que tiene los enlaces a cada una de las tablas de la base de datos
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('page_title') ?>
    <?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div>
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de las consumiciones</p>
            <div class="mb-4">
                <a href="<?= base_url('consumiciones')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Consumiciones </button>
                </a>
            </div>
        </div>
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de los detalles de los pedidos</p>
            <div class="mb-4">
                <a href="<?= base_url('detalle_pedido')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Detalles de Pedidos </button>
                </a>
            </div>
        </div>
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de los detalles de los platos</p>
            <div class="mb-4">
                <a href="<?= base_url('detalle_plato')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Detalles de Platos </button>
                </a>
            </div>
        </div>
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de las mesas</p>
            <div class="mb-4">
                <a href="<?= base_url('mesas')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Mesas </button>
                </a>
            </div>
        </div>
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de los pedidos</p>
            <div class="mb-4">
                <a href="<?= base_url('pedidos')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Pedidos </button>
                </a>
            </div>
        </div>
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de de los pedidos de los cliente</p>
            <div class="mb-4">
                <a href="<?= base_url('pedido_clientes')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Pedidos de clientes</button>
                </a>
            </div>
        </div>
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de las recomendaciones para los pedidos</p>
            <div class="mb-4">
                <a href="<?= base_url('pedido_recomendacion')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Recomendaciones de Pedido </button>
                </a>
            </div>
        </div>
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de los platos</p>
            <div class="mb-4">
                <a href="<?= base_url('platos')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Platos </button>
                </a>
            </div>
        </div>
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de los productos</p>
            <div class="mb-4">
                <a href="<?= base_url('productos')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Productos </button>
                </a>
            </div>
        </div>
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de los proveedores</p>
            <div class="mb-4">
                <a href="<?= base_url('proveedores')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Proveedores </button>
                </a>
            </div>
        </div>
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de las reservas de las mesas</p>
            <div class="mb-4">
                <a href="<?= base_url('reserva_mesa')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Reservas de las Mesas </button>
                </a>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>

<!--

        <div>
            <p class="mt-3">Podemos observar la tabla con la información de los clientes</p>
            <div class="mb-4">
                <a href="</?= base_url('clientes')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Clientes </button>
                </a>
            </div>
        </div>
        
        <div>
            <p class="mt-3">Podemos observar la tabla con la información de los tipos de mesas</p>
            <div class="mb-4">
                <a href="</?= base_url('tipo_mesa')?>" target="_blank">
                   <button class="btn btn-info" type="button"> Tipo Mesas </button>
                </a>
            </div>
        </div>

-->
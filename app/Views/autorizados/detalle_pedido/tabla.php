<?php 
/*******************************************************************************
 * Tabla de detalle pedido
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>id</th>
                    <th>Código Pedido</th>
                    <th>Código del Producto</th>
                    <th>Cantidad</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($detalle_pedidos as $detalle_pedido): ?> 
                    <tr>
                        <td><?= $detalle_pedido->id ?></td>
                        <td><?= $detalle_pedido->cod_pedido ?></td>
                        <td><?= $detalle_pedido->cod_producto ?></td>
                        <td><?= $detalle_pedido->cantidad ?></td>
                        <td>
                            <a href="<?= site_url('detalle_pedido/editar/'.$detalle_pedido->id)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('detalle_pedido/borrar/'.$detalle_pedido->id)?>" onclick="return confirm('¿Estás seguro de que quieres borrar el detalle del pedido seleccionado?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

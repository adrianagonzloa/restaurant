<?php 
/*******************************************************************************
 * Formulario de esdición de pedido
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
<!-- Esta parte será para cuando ponga la validacion -->
        <h2><?= $this->renderSection('title')?></h2>

        <?php if (!empty($errores)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errores as $field => $error): ?>
                    <p><?= $field.' - '.$error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
<!-- Formulario para el cliente -->
        <?= form_open('detalle_pedido/editar/'.$detalle_pedido->id) ?>
		<div class="form-group">
                    <?= form_label('Código del pedido:','cod_pedido')?>
                    <?= form_input('cod_pedido',$detalle_pedido->cod_pedido,['id'=>'cod_pedido','class'=>'form-control', 'readonly' => true]) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Código del Producto:','cod_producto')?>
                    <?= form_input('cod_producto',$detalle_pedido->cod_producto,['id'=>'cod_producto','class'=>'form-control', 'readonly' => true]) ?>
        	</div>
                <div class="form-group">
                    <?= form_label('Cantidad:','cantidad')?>
                    <?= form_input('cantidad',$detalle_pedido->cantidad,['id'=>'cantidad','class'=>'form-control']) ?>
		</div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>
    </div>
    

<?= $this->endSection() ?>

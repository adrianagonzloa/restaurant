<?php 
/*******************************************************************************
 * Formulario de edición de detalle_pedido
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/detalle_pedido/formdetallepedido_editar_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para detalle_pedido -->

        <?= form_open_multipart('detalle_pedido/editar/'.$detalle_pedido->id,['id'=>'detallepedido_editar']) ?> <!-- , ['id'=>'solicitud'] / form_open_multipart para añadir atributos, por ejemplo, subir archivos con ese formulario -->
            <div class="col-lg-9">
                <?= my_form_input('cod_pedido',$detalle_pedido->cod_pedido,['label' => 'Código del pedido:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly' => true])?>
            </div>

            <div class="col-lg-9">
                <?= my_form_input('cod_producto',$detalle_pedido->cod_producto,['label' => 'Código del Producto:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly' => true])?>
            </div>

            <div class="col-lg-9">
                <?= my_form_input('cantidad',$detalle_pedido->cantidad,['label' => 'Cantidad: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>

            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>
<?= $this->endSection() ?>

<?php 
/*******************************************************************************
 * Tabla de pedido de recomendación
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>Código Pedido Recomendacion</th>
                    <th>Código Producto</th>
                    <th>Cantidad de Pedido</th>
                    <th>Fecha</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($pedido_recomendaciones as $pedido_recomendacion): ?> 
                    <tr>
                        <td><?= $pedido_recomendacion->cod_ped_rec ?></td>
                        <td><?= $pedido_recomendacion->cod_producto ?></td>
                        <td><?= $pedido_recomendacion->cantidad_pedido ?></td>
                        <td><?= $pedido_recomendacion->fecha ?></td>
                        <td>
                            <a href="<?= site_url('pedido_recomendarion/editar/'.$pedido_recomendacion->cod_ped_rec)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('pedido_recomendarion/borrar/'.$pedido_recomendacion->cod_ped_rec)?>" onclick="return confirm('Estás seguro de que quieres borrar esta recomendación de pedido médico?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

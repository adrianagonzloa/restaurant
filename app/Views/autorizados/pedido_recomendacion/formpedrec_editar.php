<?php 
/*******************************************************************************
 * Formulario de edición para las recomendaciones de pedidos
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/pedido_recomendacion/formpedrec_editar') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para el pedido de recomendación -->

        <?= form_open_multipart('pedido_recomendacion/editar/'.$pedido_recomendacion->cod_ped_rec,['id'=>'pedrec_editar']) ?> <!-- , ['id'=>'solicitud'] / form_open_multipart para añadir atributos, por ejemplo, subir archivos con ese formulario -->
            <div class="col-lg-9">
                <?= my_form_input('cod_producto',$pedido_recomendacion->cod_producto,['label' => 'Código Producto:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly' => true])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('cantidad',$pedido_recomendacion->cantidad,['label'=>'Cantidad de pedido: ' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('fecha',$pedido_recomendacion->fecha,['label' => 'Fecha: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>

            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>


    

<?= $this->endSection() ?>

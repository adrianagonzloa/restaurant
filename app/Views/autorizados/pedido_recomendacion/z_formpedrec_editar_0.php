<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
<!-- Esta parte será para cuando ponga la validacion -->
        <h2><?= $this->renderSection('title')?></h2>

        <?php if (!empty($errores)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errores as $field => $error): ?>
                    <p><?= $field.' - '.$error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
<!-- Formulario para el cliente -->
        <?= form_open('pedido_recomendacion/editar/'.$pedido_recomendacion->cod_ped_rec) ?>
		<div class="form-group">
                    <?= form_label('Código Producto:','cod_producto')?>
                    <?= form_input('cod_producto',$pedido_recomendacion->cod_producto,['cod_ped_rec'=>'cod_producto','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Cantidad de pedido:','cantidad')?>
                    <?= form_input('cantidad',$pedido_recomendacion->cantidad,['cod_ped_rec'=>'cantidad','class'=>'form-control']) ?>
        	</div>
		<div class="form-group">
                    <?= form_label('Fecha:','fecha')?>
                    <?= form_input('fecha',$pedido_recomendacion->fecha,['cod_ped_rec'=>'fecha','class'=>'form-control']) ?>
		</div>


            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>
            <!--</form>-->
    </div>
    

<?= $this->endSection() ?>

<?php 
/*******************************************************************************
 * Formulario de edición del tipo de mesa
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/tipo_mesa/formtipomesa_editar_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para el tipo de mesa -->

        <?= form_open_multipart('tipo_mesa/editar/'.$tipo_mesa->capacidad,['id'=>'tipomesa_editar']) ?> <!-- , ['id'=>'solicitud'] / form_open_multipart para añadir atributos, por ejemplo, subir archivos con ese formulario -->
            <div class="col-lg-9">
                <?= my_form_input('capacidad',$tipo_mesa->capacidad,['label' => 'Capacidad:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly'=> true])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('cantidad',$tipo_mesa->cantidad,['label' => 'Cantidad: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>

            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>

<?= $this->endSection() ?>

<?php 
/*******************************************************************************
 * Tabla de los tipos de mesas
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>Capacidad</th>
                    <th>Cantidad</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($tipo_mesas as $tipo_mesa): ?> 
                    <tr>
                        <td><?= $tipo_mesa->capacidad ?></td>
                        <td><?= $tipo_mesa->cantidad ?></td>
                        <td>
                            <a href="<?= site_url('tipo_mesa/editar/'.$tipo_mesa->capacidad)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('tipo_mesa/borrar/'.$tipo_mesa->capacidad)?>" onclick="return confirm('¿Estás seguro de que quieres borrar el tipo de mesa seleccionado?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

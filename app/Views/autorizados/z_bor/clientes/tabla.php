<?php 
/*******************************************************************************
 * Tabla de los clientes
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Dirección</th>
                    <th>Ciudad</th>
                    <th>Teléfono</th>
                    <th>E-mail</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($clientes as $cliente): ?> 
                    <tr>
                        <td><?= $cliente-> cod_cliente ?></td>
                        <td><?= $cliente->nombre ?></td>
                        <td><?= $cliente->apellido ?></td>
                        <td><?= $cliente->direccion ?></td>
                        <td><?= $cliente->ciudad ?></td>
                        <td><?= $cliente->telefono ?></td>
                        <td><?= $cliente->correo_electronico ?></td>
                        <td>
                            <a href="<?= site_url('clientes/editar/'.$cliente->cod_cliente)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('clientes/borrar/'.$cliente->cod_cliente)?>" onclick="return confirm('Estás seguro de que quieres borrar el cliente seleccionado?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

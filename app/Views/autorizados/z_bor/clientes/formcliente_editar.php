<?php 
/*******************************************************************************
 * Formulario de editar los clientes
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/clientes/formcliente_editar_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para el cliente -->

        <?= form_open_multipart('clientes/editar/'.$cliente->cod_cliente,['id'=>'cliente_editar']) ?> <!-- , ['id'=>'solicitud'] / form_open_multipart para añadir atributos, por ejemplo, subir archivos con ese formulario -->
            <div class="form-group">
                <?= my_form_input('nombre',$cliente->nombre,['label' => 'Nombre: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>

            <div class="col-lg-9">
                <?= my_form_input('apellido',$cliente->apellido,['label' => 'Apellidos: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('correo_electronico',$cliente->correo_electronico,['label' => 'Email: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('direccion',$cliente->direccion,['label'=>'Dirección: ' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9 ">
                <?= my_form_input('ciudad',$cliente->ciudad,['label'=>'Ciudad: ' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class=" col-lg-3">
                <?= my_form_input('telefono',$cliente->telefono,['label' => 'Teléfono: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>

            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>

<?= $this->endSection() ?>

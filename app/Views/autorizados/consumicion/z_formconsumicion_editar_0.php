<?php 
/*******************************************************************************
 * Formulario de edición de Consumicion
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
<!-- Esta parte será para cuando ponga la validacion -->
        <h2><?= $this->renderSection('title')?></h2>

        <?php if (!empty($errores)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errores as $field => $error): ?>
                    <p><?= $field.' - '.$error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
<!-- Formulario para el cliente -->
        <?= form_open('consumiciones/editar/'.$consumicion->id) ?>
		<div class="form-group">
                    <?= form_label('Código del pedido del cliente:','cod_pedido_cliente')?>
                    <?= form_input('cod_pedido_cliente',$consumicion->cod_pedido_cliente,['id'=>'cod_pedido_cliente','class'=>'form-control', 'readonly' => true]) ?>
                    <!-- Solo se puede leer, no modificar -->
		</div>
		<div class="form-group">
                    <?= form_label('Código cliente:','cod_cliente')?>
                    <?= form_input('cod_cliente',$consumicion->cod_cliente,['id'=>'cod_cliente','class'=>'form-control', 'readonly' => true]) ?>
        	</div>
                <div class="form-group">
                    <?= form_label('Código del plato:','cod_plato')?>
                    <?= form_input('cod_plato',$consumicion->cod_plato,['id'=>'cod_plato','class'=>'form-control', 'readonly' => true]) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Precio del Plato:','precio_plato')?>
                    <?= form_input('precio_plato',$consumicion->precio_plato,['id'=>'precio_plato','class'=>'form-control']) ?>
		</div>
                <div class="form-group">
                    <?= form_label('Cantidad:','cantidad')?>
                    <?= form_input('cantidad',$consumicion->cantidad,['id'=>'cantidad','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Precio Total:','precio_total')?>
                    <?= form_input('precio_total',$consumicion->precio_total,['id'=>'precio_total','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Observaciones:','observaciones')?>
                    <?= form_input('observaciones',$consumicion->observaciones !== null ? $consumicion->observaciones : '',['id'=>'observaciones','class'=>'form-control']) ?>
                    <!-- Como Observaciones sule ser nulo pongo lo anterior, porque o si no dá error de que es un valor null -->        
                    <!--,['id'=>'observaciones','class'=>'form-control']) ?>-->
		</div>


            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>

    </div>
    

<?= $this->endSection() ?>

<?php 
/*******************************************************************************
 * Tabla de las consumiciones
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>id</th>
                    <th>Código Pedido Cliente</th>
                    <th>Código del Cliente</th>
                    <th>Código de Plato</th>
                    <th>Precio del Plato</th>
                    <th>Cantidad</th>
                    <th>Precio Total</th>
                    <th>Observaciones</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($consumiciones as $consumicion): ?> 
                    <tr>
                        <td><?= $consumicion->id  ?></td>
                        <td><?= $consumicion->cod_pedido_cliente  ?></td>
                        <td><?= $consumicion->cod_cliente ?></td>
                        <td><?= $consumicion->cod_plato ?></td>
                        <td><?= $consumicion->precio_plato ?></td>
                        <td><?= $consumicion->cantidad ?></td>
                        <td><?= $consumicion->precio_total ?></td>
                        <td><?= $consumicion->observaciones ?></td>
                        <td>
                            <a href="<?= site_url('consumiciones/editar/'.$consumicion->id)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('consumiciones/borrar/'.$consumicion->id)?>" onclick="return confirm('¿Estás seguro de que quieres borrar esta consumición seleccionada?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

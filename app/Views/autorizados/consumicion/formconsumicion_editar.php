<?php 
/*******************************************************************************
 * Formulario de edición de Consumicion
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/consumicion/formconsumicion_editar_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para la consumición -->

        <?= form_open_multipart('consumiciones/editar/'.$consumicion->id,['id'=>'consumicion_editar']) ?> <!-- , ['id'=>'solicitud'] / form_open_multipart para añadir atributos, por ejemplo, subir archivos con ese formulario -->
            <div class="col-lg-9">
                <?= my_form_input('cod_pedido_cliente',$consumicion->cod_pedido_cliente,['label' => 'Código del pedido del cliente:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly' => true])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('cod_cliente',$consumicion->cod_cliente,['label' => 'Código cliente:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly' => true])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('cod_plato',$consumicion->cod_plato,['label' => 'Código del plato:' ,'class'=>'form-control', 'errors'=>$errors, 'readonly' => true])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('precio_plato',$consumicion->precio_plato,['label' => 'Precio del Plato: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('cantidad',$consumicion->cantidad,['label' => 'Cantidad: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('precio_total',$consumicion->precio_total,['label'=>'Precio Total: ' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9 ">
                <?= my_form_input('observaciones',$consumicion->observaciones !== null ? $consumicion->observaciones : '',['label'=>'Observaciones: ' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>

            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>
<?= $this->endSection() ?>

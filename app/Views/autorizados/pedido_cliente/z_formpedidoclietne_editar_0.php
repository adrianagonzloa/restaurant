<?php 
/*******************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte
 * 
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
<!-- Esta parte será para cuando ponga la validacion -->
        <h2><?= $this->renderSection('title')?></h2>

        <?php if (!empty($errores)): ?>
            <div class="alert alert-danger">
                <?php foreach ($errores as $field => $error): ?>
                    <p><?= $field.' - '.$error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
<!-- Formulario para el cliente -->
        <?= form_open('pedido_clientes/editar/'.$pedido_cliente->cod_pedido_cliente) ?>
		<div class="form-group">
                    <?= form_label('Fecha:','fecha')?>
                    <?= form_input('fecha',$pedido_cliente->fecha,['cod_pedido_cliente'=>'fecha','class'=>'form-control']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('Precio:','precio')?>
                    <?= form_input('precio',$pedido_cliente->precio,['cod_pedido_cliente'=>'precio','class'=>'form-control']) ?>
        	</div>
		<div class="form-group">
                    <?= form_label('Estado:','estado')?>
                    <?= form_input('estado',$pedido_cliente->estado,['cod_pedido_cliente'=>'estado','class'=>'form-control']) ?>
		</div>


            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
        <?= form_close() ?>
            <!--</form>-->
    </div>
    

<?= $this->endSection() ?>

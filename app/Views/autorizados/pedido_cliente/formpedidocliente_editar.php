<?php 
/*******************************************************************************
 * Formulario de edicion del pedido del cliente
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('js') ?>
    <?= $this->include('autorizados/pedido_cliente/formpedidocliente_editar_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <div class="container">
    	
        <!-- Mostrar los errores si los hubiera -->
        <?php if (isset($errors)) : ?>

        <?php else: ?>
            <?php $errors=[] ?>
        <?php endif ?>

        <!-- Formulario para el pedido del cliente -->

        <?= form_open_multipart('pedido_clientes/editar/'.$pedido_cliente->cod_pedido_cliente,['id'=>'pedidoclietne_editar']) ?> 
            <div class="col-lg-9">
                <?= my_form_input('fecha',$pedido_cliente->fecha,['label' => 'Fecha: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= my_form_input('precio',$pedido_cliente->precio,['label'=>'Precio: <code>*</code>' ,'class'=>'form-control', 'errors'=>$errors])?>
            </div>
            <div class="col-lg-9">
                <?= form_label('Estado:  <code>*</code>','estado')?>
                <?= form_dropdown('estado', ['Pagado' => 'Pagado', 'En Proceso' => 'En Proceso'], $pedido_cliente->estado, 'class="form-control"', 'errors=$errors') ?>
            </div>

            <div class="col-12">
                <code class="small"> * obligatorio</code>
            </div>

            <!--Boton para enviar la informacion-->
            <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary mt-4']) ?>
        <?= form_close() ?>
    </div>

<?= $this->endSection() ?>

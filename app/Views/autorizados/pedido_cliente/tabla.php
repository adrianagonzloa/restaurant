<?php 
/*******************************************************************************
 * Tabla de pedido de los clientes
 ******************************************************************************/
?>

<?= $this->extend('plantillas/plantilla1') ?>

<?= $this->section('css') ?>
    <?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
    <?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    
    <div class="container">
        <table id="myTable" class="table table-striped table-bordered bg-white" style="width: 100%"> 
            <thead>
                <tr>
                    <th>Código Pedido Cliente</th>
                    <th>Fecha</th>
                    <th>Precio</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($pedido_clientes as $pedido_cliente): ?> 
                    <tr>
                        <td><?= $pedido_cliente->cod_pedido_cliente?></td>
                        <td><?= $pedido_cliente->fecha ?></td>
                        <td><?= $pedido_cliente->precio ?></td>
                        <td><?= $pedido_cliente->estado ?></td>
                        <td>
                            <a href="<?= site_url('pedido_clientes/editar/'.$pedido_cliente->cod_pedido_cliente)?>">
                                <span class="bi bi-pencil"></span></a>
                            <a href="<?= site_url('pedido_clientes/borrar/'.$pedido_cliente->cod_pedido_cliente)?>" onclick="return confirm('¿Estás seguro de que quieres borrar el pedido cliente seleccionado?')">
                                <span class="bi bi-trash text-danger"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?= $this->endSection() ?>

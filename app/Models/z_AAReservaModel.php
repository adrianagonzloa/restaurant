<?php

namespace App\Models;

use CodeIgniter\Model;
use DateTime;
use DateInterval;
use DatePeriod;

class AAReservaModel extends Model {

    protected $table = 'aa_reservas';
    protected $primaryKey = 'id';
    protected $useAutoIncrement = true;
    protected $returnType = 'object';
   
/*******************************************************************************
 * Devuelve un número que indica
 * false: está libre
 * true: está ocupada
 *  entre dos dadas para un recurso
 *******************************************************************************/       
    public function estaReservada($recurso, $inicio, $fin){
        //me indica si hay algún registro en la tabla que solape con las fechas
        $resultado = $this->builder()->where(['id_recurso'=>$recurso,'fin >='=> $inicio,'inicio <='=> $fin])->get(); 
        $registros = $resultado->getResult();
        if (empty($registros)){
            return false; //está disponible
        } else {
            return true; //
        }
    }
    
/*******************************************************************************
 * Devuelve un array de fechas ocupadas entre dos dadas para un recurso
 *******************************************************************************/    
    public function getReservas($recurso, $inicio, $fin){
        $resultado = $this->builder()->where(['id_recurso'=>$recurso,'fin >='=> $inicio,'inicio <='=> $fin])->get(); 
        $registros = $resultado->getResult();
        $reservas = [];
        foreach ($registros as $registro) {
            $end = new DateTime($registro->fin);
            $end->setTime(0, 0, 1); //para incluir el último día del intervalo
            $periodo = new DatePeriod(new DateTime($registro->inicio), new DateInterval('P1D'), $end); //está todo inventado
            foreach ($periodo as $value) {
                $dia = $value->format('Y-m-d');
                if ($dia >=$inicio AND $dia <= $fin){
                    $reservas [] = $dia;
                }
            }
        }
        return $reservas;
    }
/*******************************************************************************
 *  Buscamos los recursos disponibles en una fecha dada
 *******************************************************************************/    
    
    public function getDisponibles($inicio, $fin){
        $resultado = $this->builder()->where(['fin >='=> $inicio,'inicio <='=> $fin])->get();  //solapamientos
        $registros = $resultado->getResult();
        return $registros;
    }

    /*******************************************************************************
 * Devuelve un array de fechas ocupadas (todos los recursos de ese tipo) entre
 * dos dadas para un tipo de recurso.
 * Entre desde hoy hasta el mes próximo.
 * devuelve las reservas para cada turno y el máximo de las posibles
 *
 * SELECT `inicio`, `cantidad`, `id_turno`,COUNT(`id_turno`) AS `reservas`
 * FROM `aa_reservas`
 * LEFT JOIN `aa_recursos` ON `aa_recursos`.`id`=`aa_reservas`.`id_recurso`
 * WHERE `id_recurso` = 1 AND `inicio` >= '2024-05-02' AND `inicio` <= '2024-06-02'
 * GROUP BY `inicio`, `id_turno`
 * HAVING count('aa_reservas.id_turno') < aa_recursos.cantidad
 *
 *******************************************************************************/    
    
    public function getReservasMesa($recurso){
    	$hoy = date('Y-m-d');
    	$mesqueviene = date('Y-m-d',strtotime("+1 month", strtotime($hoy)));
$sql = <<< SQL
SELECT inicio,COUNT(inicio) as totalturnos
FROM
	(SELECT `inicio`, `cantidad`, `id_turno`,COUNT(`id_turno`) AS `reservas`
 	FROM `aa_reservas`
 	LEFT JOIN `aa_recursos` ON `aa_recursos`.`id`=`aa_reservas`.`id_recurso`
 	WHERE `id_recurso` = :recurso: AND `inicio` >= :hoy: AND `inicio` <= :mesqueviene:
 	GROUP BY `inicio`, `id_turno`
 	HAVING count('aa_reservas.id_turno') = aa_recursos.cantidad) as reservas
GROUP BY inicio
HAVING COUNT(inicio) = (SELECT count(id) FROM aa_turnos)
SQL;
    	$resultado = $this->db->query($sql,['recurso'=>$recurso, 'hoy'=>$hoy, 'mesqueviene'=>$mesqueviene]);
   	
    	$registros = $resultado->getResult();
    	$reservas = [];
    	foreach ($registros as $registro) {
            	$reservas [] = $registro;
    	}
    	return $reservas;
    }
    
    public function estaTurnoCompleto($recurso, $fecha, $turno){
    	//me indica si hay algún registro en la tabla que solape con las fechas
    	$resultado = $this->builder()
        	->select('id_turno,aa_recursos.cantidad')
        	->join('aa_recursos','aa_recursos.id=aa_reservas.id_recurso','LEFT')
        	->where(['id_recurso'=>$recurso,'inicio'=> $fecha, 'id_turno'=>$turno])
        	->having("count('aa_reservas.id_turno')",'aa_recursos.cantidad',false) //solo las que no quede mesa de ese tipo
        	->get();
        	//->getCompiledSelect();
    	$registros = $resultado->getResult();
    	if (!empty($registros)){
        	return true; //está disponible si encontramos una completo
    	} else {
        	return false; //
    	}
    }
    /*
    public function getReservasMesa1($recurso){
    	$hoy = date('Y-m-d');
    	$mesqueviene = date('Y-m-d',strtotime("+1 month", strtotime($hoy)));
    	$resultado = $this->builder()
            ->select('inicio,cantidad,id_turno')
            ->selectCount('id_turno','reservas')
            ->join('aa_recursos','aa_recursos.id=aa_reservas.id_recurso','LEFT')
            ->where(['id_recurso'=>$recurso,'inicio >='=> $hoy,'inicio <='=> $mesqueviene])
            ->groupBy('inicio,id_turno')
            ->having("count('aa_reservas.id_turno')",'aa_recursos.cantidad',false) //solo las que no quede mesa de ese tipo
            //->getCompiledSelect();
            ->get();
   	 
    	$registros = $resultado->getResult();
    	$reservas = [];
    	foreach ($registros as $registro) {
            	$reservas [] = $registro;
    	}
    	return $reservas;
    	//return $resultado;
    }
    
    */
    
    public function turnosLibres(){
    	helper('form');
    	$recurso = $this->request->getPost('recurso');
    	$fecha = $this->request->getPost('fecha'); //recogemos los dos parámetros como si viniera de un formulario
    	$turnoModel = new AATurnoModel();
    	$reservaModel = new AAReservaModel();
    	$todos_turnos = $turnoModel->findAll();
    	$turnos = [];
    	foreach($todos_turnos as $turno){
            if (!$reservaModel->estaTurnoCompleto($recurso, $fecha, $turno->id)){
            $turnos[] = $turno;
            }
    	}
    	echo '<pre>';
    	print_r($turnos);
    }
        
}

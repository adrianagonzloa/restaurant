<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */
namespace App\Models;
use CodeIgniter\Model; //Estos dos importantes, si o están no funciona
/**
 * Description of ProductoModel
 *
 * @author adriana
 */
class ProductoModel extends Model{ //Aquí lo lamamos, lo de arriba de Model
    protected $table = 'producto'; //tabla cliente
    protected $primaryKey = 'cod_producto'; // clave primaria
    protected $useAutoIncrement = false; //No es autoincremental porque es VARCHAR
    protected $returnType = 'object'; //array o object
    protected $allowedFields = ['nombre','categoria','unidades_stock','presentacion','razon','precio','stock_minimo','pedido','stock']; //campos de la tabla menos PrimaryKey

    // VALIDACIÓN
    //Reglas de validación para cada campo
    protected $validationRules = [
        'nombre' => 'required|max_length[100]|min_length[3]|is_unique',
        'categoria' => 'required|max_length[20]|min_length[3]',
        'unidades_stock' => 'required|max_length[20]|min_length[1]',
        'presentacion' => 'required|max_length[250]|min_length[3]',
        'razon' => 'required|numeric|max_length[10]|min_length[1]',
        'precio' => 'required|numeric|max_length[10]|min_length[1]',//?????????????
        'stock_minimo' => 'required|numeric|max_length[10]|min_length[1]',
        'pedido' => 'required|numeric|max_length[10]|min_length[1]',
        'stock' => 'required|numeric|max_length[10]|min_length[1]',
        
        //'email' => 'required|max_length[254]|valid_email|is_unique[clientes.email,id,{id}]', //Aquí hay que poner esto del id tmb
    ];
    //Mensajes de validación
    protected $validationMessages = [
        'nombre' => [
            'is_unique' => 'Ya existe un producto llamado así.',
            'required' => 'Debes especificar el nombre.',
            'min_length' => 'El nombre debe ser superior a 3 carácteres.',
            'max_length' => 'Este nombre excede los caracteres máximos.'
        ],'categoria' => [
            'is_unique' => 'Ya existe un producto llamado así.'
        ],'unidades_stock' => [
            'is_unique' => 'Ya existe un producto llamado así.'
        ],'presentacion' => [
            'is_unique' => 'Ya existe un producto llamado así.'
        ],'razon' => [
            'is_unique' => 'Ya existe un producto llamado así.'
        ],'precio' => [
            'is_unique' => 'Ya existe un producto llamado así.'
        ],'stock_minimo' => [
            'is_unique' => 'Ya existe un producto llamado así.'
        ],'pedido' => [
            'is_unique' => 'Ya existe un producto llamado así.'
        ],'stock' => [
            'is_unique' => 'Ya existe un producto llamado así.'
        ],
    ];

    
}

<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */
namespace App\Models;
use CodeIgniter\Model; //Estos dos importantes, si o están no funciona
/**
 * Description of PedidoRecomendacionModel
 *
 * @author adriana
 */
class PedidoRecomendacionModel extends Model{ //Aquí lo lamamos, lo de arriba de Model
    protected $table = 'pedido_recomendacion'; //tabla cliente
    protected $primaryKey = 'cod_ped_rec'; // clave primaria
    protected $useAutoIncrement = false; //No es autoincremental porque es VARCHAR
    protected $returnType = 'object'; //array o object
    protected $allowedFields = ['cod_producto','cantidad_pedido','fecha']; //campos de la tabla menos PrimaryKey
}

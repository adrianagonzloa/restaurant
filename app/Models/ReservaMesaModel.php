<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */
namespace App\Models;
use CodeIgniter\Model; //Estos dos importantes, si o están no funciona
/**
 * Description of ReservaMesaModel
 *
 * @author adriana
 */
class ReservaMesaModel extends Model{ //Aquí lo lamamos, lo de arriba de Model
    protected $table = 'reserva_mesa'; //tabla reserva_mesa
    protected $primaryKey = 'id'; // clave primaria
    protected $useAutoIncrement = true; //Si que es AI
    protected $returnType = 'object'; //array o object
    protected $allowedFields = ['cod_reserva','cod_cliente','cod_mesa','fecha','cod_turno','cantidad_personas','estado']; //campos de la tabla menos PrimaryKey

    
    
    public function getReservasMesa($recurso){
    	$hoy = date('Y-m-d');
    	$mesqueviene = date('Y-m-d',strtotime("+1 month", strtotime($hoy)));
        $sql = <<< SQL
            SELECT fecha,COUNT(fecha) as totalturnos
            FROM
                (SELECT `fecha`, `cantidad`, `cod_turno`,COUNT(`cod_turno`) AS `reservas`
                    FROM `reserva_mesa`
                    LEFT JOIN `mesa` ON `mesa`.`cod_mesa`=`reserva_mesa`.`cod_mesa`
                    WHERE `reserva_mesa`.`cod_mesa` = :recurso: AND `fecha` >= :hoy: AND `fecha` <= :mesqueviene:
                    GROUP BY `fecha`, `cod_turno`
                    HAVING count('reserva_mesa.cod_turno') = mesa.cantidad) 
                as reservas
            GROUP BY fecha
            HAVING COUNT(fecha) = (SELECT count(cod_turno) FROM turno)
        SQL;
    	$resultado = $this->db->query($sql,['recurso'=>$recurso, 'hoy'=>$hoy, 'mesqueviene'=>$mesqueviene]);
   	 
    	$registros = $resultado->getResult();
    	$reservas = [];
    	foreach ($registros as $registro) {
            	$reservas [] = $registro;
    	}
    	return $reservas;
    }
    
    public function estaTurnoCompleto($recurso, $fecha, $turno){
        //me indica si hay algún registro en la tabla que solape con las fechas
    	$resultado = $this->builder()
            ->select('cod_turno,mesa.cantidad')
            ->join('mesa','mesa.cod_mesa=reserva_mesa.cod_mesa','LEFT')
            ->where(['mesa.cod_mesa'=>$recurso,'fecha'=> $fecha, 'cod_turno'=>$turno])
            ->where('reserva_mesa.estado!=', 'Cancelada') // Excluir reservas con estado "Cancelada"
            ->having("count('reserva_mesa.cod_turno')",'mesa.cantidad',false) //solo las que no quede mesa de ese tipo
            ->get();
            //->getCompiledSelect();
//        $resultado = $this->builder()
//            ->select('cod_turno, mesa.cantidad')
//            ->join('mesa', 'mesa.cod_mesa = reserva_mesa.cod_mesa', 'LEFT')
//            ->where(['mesa.cod_mesa' => $recurso, 'fecha' => $fecha, 'cod_turno' => $turno])
//            ->where('estado !=', 'Cancelada') // Excluir reservas con estado "Cancelada"
//            ->having("count('reserva_mesa.cod_turno')", 'mesa.cantidad', false)
//            ->get();
    	$registros = $resultado->getResult();
    	if (!empty($registros)){
        	return true; //está disponible si encontramos una completo
    	} else {
        	return false; //
    	}
    }
    
    public function turnosLibres(){
    	helper('form');
    	$recurso = $this->request->getPost('recurso');
    	$fecha = $this->request->getPost('fecha'); //recogemos los dos parámetros como si viniera de un formulario
    	$turnoModel = new TurnoModel();
    	$reservamesaModel = new ReservaMesaModel();
    	$todos_turnos = $turnoModel->findAll();
    	$turnos = [];
    	foreach($todos_turnos as $turno){
            if (!$reservamesaModel->estaTurnoCompleto($recurso, $fecha, $turno->cod_turno)){
            $turnos[] = $turno;
            }
    	}
    	echo '<pre>';
    	print_r($turnos);
    }
    
    // VALIDACIÓN
    //Reglas de validación para cada campo
//    protected $validationRules = [
//        'cod_cliente' => 'required|is_not_unique[users.id]',
//        'cod_mesa' => 'required|is_not_unique[mesa.cod_mesa]',
//        'fecha' => 'required|valid_date',
//        'cod_turno' => 'required|is_not_unique[turno.cod_turno]',
//        'cantidad_personas' => 'numeric|max_length[2]|min_length[1]',
//        'estado' => 'required|in_list[Confirmada,Cancelada]',        
//    ];
//    //Mensajes de validación
//    protected $validationMessages = [
//        'cod_cliente' => [
//            'required' => 'Debes especificar el cliente.',
//        ],
//        'cod_mesa' => [
//            'required' => 'Debes especificar una mesa.',
//        ],
//        'fecha' => [
//            'required' => 'Debes especificar el cliente.',
//            'valid_date' => 'Debes especificar una fecha valida.',
//        ],
//        'cod_turno' => [
//            'required' => 'Debes especificar el turno.',
//        ],
//        'cantidad_personas' => [
//            'numeric' => 'Debes especificar un número.',
//            'max_length' => 'No puede superar los 2 caracteres.',
//            'min_length' => 'No puede ser inferior a 1 caracteres.'
//        ],
//        'estado' => [
//            'in_list' => 'Tienes que especificar una de las dos opciones, Confirmada o Cancelada.',
//        ],
//    ];
    
}

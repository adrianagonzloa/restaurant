<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */
namespace App\Models;
use CodeIgniter\Model; //Estos dos importantes, si o están no funciona
/**
 * Description of MesaModel
 *
 * @author adriana
 */
class MesaModel extends Model{ //Aquí lo lamamos, lo de arriba de Model
    protected $table = 'mesa'; //tabla cliente
    protected $primaryKey = 'cod_mesa'; // clave primaria
    protected $useAutoIncrement = false; //No es autoincremental porque es VARCHAR
    protected $returnType = 'object'; //array o object
    protected $allowedFields = ['capacidad','estado']; //campos de la tabla menos PrimaryKey
}

<?php

namespace App\Models;

use CodeIgniter\Model;

class AATurnoModel extends Model {

    protected $table = 'aa_turnos';
    protected $primaryKey = 'id';
    protected $useAutoIncrement = true;
    protected $returnType = 'object';
   
}

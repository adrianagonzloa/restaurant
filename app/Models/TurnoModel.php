<?php

namespace App\Models;

use CodeIgniter\Model;

class TurnoModel extends Model {

    protected $table = 'turno';
    protected $primaryKey = 'cod_turno'; // clave primaria
    protected $useAutoIncrement = true; //No es autoincremental porque es VARCHAR
    protected $returnType = 'object'; //array o object
    protected $allowedFields = ['turno','horario']; //campos de la tabla menos PrimaryKey
}

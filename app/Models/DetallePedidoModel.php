<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */
namespace App\Models;
use CodeIgniter\Model; //Estos dos importantes, si o están no funciona
/**
 * Description of DetallePedidoModel
 *
 * @author adriana
 */
class DetallePedidoModel extends Model{ //Aquí lo lamamos, lo de arriba de Model
    protected $table = 'detalle_pedido'; //tabla cliente
    protected $primaryKey = 'id'; // clave primaria
    protected $useAutoIncrement = true; //No es autoincremental porque es VARCHAR
    protected $returnType = 'object'; //array o object
    protected $allowedFields = ['cod_pedido','cod_producto','cantidad']; //campos de la tabla menos PrimaryKey
}

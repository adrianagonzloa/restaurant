<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */
namespace App\Models;
use CodeIgniter\Model; //Estos dos importantes, si o están no funciona
/**
 * Description of ClienteModel
 *
 * @author adriana
 */
class ClienteModel extends Model{ //Aquí lo lamamos, lo de arriba de Model
    protected $table = 'cliente'; //tabla cliente
    protected $primaryKey = 'cod_cliente'; // clave primaria
    protected $useAutoIncrement = false; //No es autoincremental porque es VARCHAR
    protected $returnType = 'object'; //array o object
    protected $allowedFields = ['nombre','apellido','direccion','ciudad','telefono','correo_electronico']; //campos de la tabla menos PrimaryKey
    
    protected $validationRules = [
        'nombre'     => 'required|max_length[30]|min_length[3]',
        ];
}

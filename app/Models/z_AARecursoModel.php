<?php

namespace App\Models;

use CodeIgniter\Model;


class RecursoModel extends Model {

    protected $table = 'aa_recursos';
    protected $primaryKey = 'id';
    protected $useAutoIncrement = true;
    protected $returnType = 'object';   
}

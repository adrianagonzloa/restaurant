<?php

use CodeIgniter\Router\RouteCollection;
/**
 * @var RouteCollection $routes
 */

$routes->get('grupos/(:num)', 'Prueba::grupos/$1');

//Autentificación
service('auth')->routes($routes);

/*Páginas Principales de la web*/
$routes->get('/', 'Home::paginainicial');
$routes->get('contacto', 'Home::contacto');
$routes->get('reserva', 'Home::form_fechahora');

//Conjunto de rutas para Carta
$routes->group('carta', static function ($routes) {    
    $routes->get('/', 'Home::carta');
    $routes->get('entrantes', 'CartaController::entrantes');
    $routes->get('principal', 'CartaController::principal');
    $routes->get('postres', 'CartaController::postres');
    $routes->get('bebida', 'CartaController::bebida');
});

/*******************************
 *** Páginas con restricción ***
 *******************************/
$routes->group('empleados', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'Home::autorizados');
});

/*Tablas*/
/*Tabla, Form Editar y Borrar*/
//Conjunto de rutas para Clientes
//$routes->group('clientes', ['filter' => 'group:superadmin,admin'], static function ($routes) {
//    $routes->get('/', 'ClienteController::index');
//    $routes->match(['get', 'post'], 'editar/(:alphanum)', 'ClienteController::formCliente_editar/$1');
//    $routes->get('borrar/(:alphanum)','ClienteController::borrarcliente/$1');
//});
//Conjunto de rutas para Mesa
$routes->group('mesas', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'MesaController::index');
    $routes->match(['get', 'post'], 'editar/(:alphanum)', 'MesaController::formmesa_editar/$1');
    $routes->get('borrar/(:alphanum)','MesaController::borrarmesa/$1');
});
//Conjunto de rutas para Consumicion
$routes->group('consumiciones', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'ConsumicionController::index');
    $routes->match(['get', 'post'], 'editar/(:num)', 'ConsumicionController::formconsumicion_editar/$1');
    $routes->get('borrar/(:num)','ConsumicionController::borrarconsumicion/$1');
});
//Conjunto de rutas para Detalle Pedido
$routes->group('detalle_pedido', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'DetallePedidoController::index');
    $routes->match(['get', 'post'], 'editar/(:num)', 'DetallePedidoController::formdetallepedido_editar/$1');
    $routes->get('borrar/(:num)','DetallePedidoController::borrardetallepedido/$1');
});
//Conjunto de rutas para Detalle Plato
$routes->group('detalle_plato', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'DetallePlatoController::index');
    $routes->match(['get', 'post'], 'editar/(:num)', 'DetallePlatoController::formdetalleplato_editar/$1');
    $routes->get('borrar/(:num)','DetallePlatoController::borrardetalleplato/$1');
});
//Conjunto de rutas para Pedido
$routes->group('pedidos', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'PedidoController::index');
    $routes->match(['get', 'post'], 'editar/(:alphanum)', 'PedidoController::formpedido_editar/$1');
    $routes->get('borrar/(:alphanum)','PedidoController::borrarpedido/$1');
});
//Conjunto de rutas para Pedido Cliente
$routes->group('pedido_clientes', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'PedidoClienteController::index');
    $routes->match(['get', 'post'], 'editar/(:alphanum)', 'PedidoClienteController::formpedidocliente_editar/$1');
    $routes->get('borrar/(:alphanum)','PedidoClienteController::borrarpedidocliente/$1');
});
//Conjunto de rutas para Pedido Recomendacion
$routes->group('pedido_recomendacion', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'PedidoRecomendacionController::index');
    $routes->get('borrar/(:alphanum)','PedidoRecomendacionController::borrarpedrec/$1');
    $routes->match(['get', 'post'], 'editar/(:alphanum)', 'PedidoRecomendacionController::formpedrec_editar/$1');
});
//Conjunto de rutas para Plato
$routes->group('platos', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'PlatoController::index');
    $routes->get('borrar/(:alphanum)','PlatoController::borrarplato/$1');
    $routes->match(['get', 'post'], 'editar/(:alphanum)', 'PlatoController::formplato_editar/$1');
});
//Conjunto de rutas para Producto
$routes->group('productos', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'ProductoController::index');
    $routes->get('borrar/(:alphanum)','ProductoController::borrarproducto/$1');
    $routes->match(['get', 'post'], 'editar/(:alphanum)', 'ProductoController::formproducto_editar/$1');
});
//Conjunto de rutas para Proveedor
$routes->group('proveedores', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'ProveedorController::index');
    $routes->get('borrar/(:alphanum)','ProveedorController::borrarproveedor/$1');
    $routes->match(['get', 'post'], 'editar/(:alphanum)', 'ProveedorController::formproveedor_editar/$1');
});
//Conjunto de rutas para Reserva Mesa
$routes->group('reserva_mesa', ['filter' => 'group:superadmin,admin'], static function ($routes) {
    $routes->get('/', 'ReservaMesaController::index');
    $routes->get('borrar/(:alphanum)','ReservaMesaController::borrarreservamesa/$1');
    $routes->match(['get', 'post'], 'editar/(:alphanum)', 'ReservaMesaController::formpreservamesa_editar/$1');
});
//Conjunto de rutas para Tipo Mesa
//$routes->group('tipo_mesa', ['filter' => 'group:superadmin,admin'], static function ($routes) {
//    $routes->get('/', 'TipoMesaController::index');
//    $routes->get('borrar/(:alphanum)','TipoMesaController::borrartipomesa/$1');
//    $routes->match(['get', 'post'], 'editar/(:alphanum)', 'TipoMesaController::formtipomesa_editar/$1');
//});

//Reserva
$routes->group('reserva', static function ($routes) {
    $routes->get('elegir_personas', 'ReservaController::cantidadPersonas');
    $routes->post('elegir_mesa', 'ReservaController::personasReserva');
    $routes->get('form/(:num)','ReservaController::formReservar/$1');
    $routes->post('turnoslibres','ReservaController::turnosLibres');
    $routes->get('(:num)/(:num)/(:num)/(:segment)', 'ReservaController::guardarReserva/$1/$2/$3/$4');
//    $routes->get('mis_reservas/(:num)','ReservaController::tabla_cliente/$1');
    $routes->post('ultima_reserva','ReservaController::ultima_reserva');
    $routes->post('cancelar_reserva','ReservaController::cancelar_reserva');
    

});

$routes->get('reservas/usuario/(:num)/(:num)/(:segment)', 'ReservaController::guardarReserva/$1/$2/$3');




/*************
 *************
 ***PRUEBAS***
 *************
 *************/
//$routes->get('fechayhora','MyHome::formTempus');
$routes->match(['get', 'post'], 'tempus/form', 'MyHome::formTempus');
$routes->match(['get', 'post'], 'tempus/calendar', 'MyHome::calendar');
$routes->match(['get', 'post'], 'tempus/reservas', 'MyHome::getReservas');
$routes->match(['get', 'post'], 'tempus/disponibles', 'MyHome::getDisponibles');
$routes->match(['get', 'post'], 'tempus/libres', 'MyHome::getLibres');

$routes->post('ajax/buscar','MyHome::buscar');
$routes->get('ajax/form','MyHome::form_ajax');
$routes->get('erimagen','Home::erimagen');

//$routes->get('reservas/form','ReservaController::formReservar1');
//$routes->post('reservas/turnoslibres','ReservaController::turnosLibres1');

/*Página de solicitud*/
//$routes->get('solicitud', 'Home::solicitud');


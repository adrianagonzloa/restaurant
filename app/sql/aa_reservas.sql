-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 30-04-2024 a las 17:38:32
-- Versión del servidor: 10.5.23-MariaDB-0+deb11u1
-- Versión de PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ulises_dev`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aa_reservas`
--

CREATE TABLE `aa_reservas` (
  `id` int(11) NOT NULL,
  `id_recurso` int(11) NOT NULL,
  `inicio` date NOT NULL,
  `fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `aa_reservas`
--

INSERT INTO `aa_reservas` (`id`, `id_recurso`, `inicio`, `fin`) VALUES
(1, 1, '2024-05-05', '2024-05-08'),
(2, 1, '2024-05-19', '2024-05-24'),
(3, 2, '2024-05-22', '2024-05-25'),
(4, 2, '2024-05-01', '2024-05-05'),
(5, 3, '2024-05-17', '2024-05-22'),
(6, 4, '2024-05-21', '2024-05-27'),
(7, 3, '2024-05-01', '2024-05-06'),
(8, 3, '2024-05-10', '2024-05-15');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aa_reservas`
--
ALTER TABLE `aa_reservas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aa_reservas_ibfk_1` (`id_recurso`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aa_reservas`
--
ALTER TABLE `aa_reservas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aa_reservas`
--
ALTER TABLE `aa_reservas`
  ADD CONSTRAINT `aa_reservas_ibfk_1` FOREIGN KEY (`id_recurso`) REFERENCES `aa_recursos` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
